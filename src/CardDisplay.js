import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";

const DIV_STYLE_PADDING = {
  all_around_padding: {
    padding: 20
  }
};

class CardDisplay extends Component {
  render() {
    var items = this.props.items;
    if (items === undefined || items.length === 0) {
      return null;
    }

    const url = this.props.url;

    switch (url) {
      case "/cities/":
        items.forEach(item => (item.href = url + item.city_id));
        break;
      case "/hospitals/":
        items.forEach(item => (item.href = url + item.hospital_id));
        break;
      case "/outdoor_facilities/":
        items.forEach(item => (item.href = url + item.facility_id));
        break;
      default:
        break;
    }

    const cardsToRender = items.map(item => (
      <Card className="text-center" border="primary">
        <Card.Header>{item.name}</Card.Header>
        <Card.Img src={item.image_url}></Card.Img>
        <Card.Footer>
          <Card.Link href={item.href}> See More </Card.Link>
        </Card.Footer>
      </Card>
    ));

    return (
      <div>
        <div className="text-center">
          <span style={DIV_STYLE_PADDING.all_around_padding}></span>
        </div>
        <CardColumns>{cardsToRender}</CardColumns>
      </div>
    );
  }
}

export default CardDisplay;
