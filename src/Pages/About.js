import React, { Component } from "react";
import { Helmet } from "react-helmet";

import alex from "../img/developers/alex.jpg";
import hyuk from "../img/developers/hyuk.png";
import david from "../img/developers/david.jpg";
import marshall from "../img/developers/marshall.png";
import yujin from "../img/developers/yujin.jpeg";
import jenn from "../img/developers/jenn.JPG";

import aws from "../img/tools/aws.png";
import bootstrap from "../img/tools/bootstrap.png";
import docker from "../img/tools/docker.png";
import flask from "../img/tools/flask.png";
import gitlab from "../img/tools/gitlab.png";
import mocha from "../img/tools/mocha.png";
import plantuml from "../img/tools/plantuml.png";
import postgresql from "../img/tools/postgresql.png";
import postman from "../img/tools/postman.png";
import react from "../img/tools/react.png";
import selenium from "../img/tools/selenium.png";
import sqlalchemy from "../img/tools/sqlalchemy.png";

const TAB_TITLE = "About";

class About extends Component {
  componentDidMount() {
    const PROJECT_ID = 16985041;
    const userToName = {
      alexandercantrell: ["alexandercantrell", "Alexander Cantrell"],
      hjchoi3: ["hjchoi3", "Hyuk Jun Choi"],
      dhuang16: ["dhuang16", "David Huang"],
      klansekmarshall: [
        "klansekmarshall",
        "Marshall Klansek",
        "Jeffrey Klansek"
      ],
      yujinseo: ["yujinseo", "Yujin Seo"],
      "jennifer-zheng": ["Jennifer-Zheng", "Jennifer Zheng"]
    };

    fetch(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics`)
      .then(res => res.json())
      .then(res => {
        document.getElementById(`all-issues`).textContent =
          res.statistics.counts.all;
      });

    Object.keys(userToName).forEach(user => {
      fetch(
        `https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics?assignee_username=${user}`
      )
        .then(res => res.json())
        .then(res => {
          document.getElementById(`issues-for-${user}`).textContent =
            res.statistics.counts.all;
        });
    });

    fetch(
      `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/contributors`
    )
      .then(res => res.json())
      .then(res => {
        const stats = Array.from(res);
        Object.keys(userToName).forEach(user => {
          document.getElementById(
            `commits-for-${user}`
          ).textContent = stats
            .filter(stat => userToName[user].includes(stat.name))
            .reduce((commits, stat) => commits + stat.commits, 0);
        });
        document.getElementById("all-commits").textContent = stats.reduce(
          (commits, stat) => commits + stat.commits,
          0
        );
      });
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>{TAB_TITLE}</title>
        </Helmet>
        <center>
          <h1>About</h1>
          <h3>Welcome to Metro Health!</h3>
          <a href="https://youtu.be/tracS4x6hMg">Presentation</a>
        </center>
        <justify>
          <div
            id="description"
            style={{
              marginTop: "20px",
              marginLeft: "20px",
              marginRight: "20px",
              marginBottom: "50px"
            }}
          >
            Our mission is to provide information on factors affecting public
            health in American cities. We provide various data points for
            cities, including water/air quality, parks/recreation spots, and
            hospitals/emergency centers. This site is for people who prioritize
            healthy living. By integrating these disparate data, we aim to
            provide users with knowledge across a wide variety of public health
            factors for cities that they are interested in living in.
          </div>
        </justify>
        <center>
          <h3>Meet Our Developers</h3>
          <div class="container-fluid">
            <div class="row">
              <div class="card-deck" style={{ margin: "auto" }}>
                <div class="card" style={{ width: "400px" }} align="left">
                  <div class="card-header" align="center">
                    <h4>Alexander Cantrell</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={alex}
                    alt="alex"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      A Senior computer science major at the University of Texas
                      at Austin who enjoys cooking, working out, and studying
                      foreign languages.
                    </p>
                    <p>
                      <b>Role:</b> AWS systems administrator and Gitlab pipeline
                      manager
                    </p>
                    <li>
                      <span id="issues-for-alexandercantrell"></span> assigned
                      issues
                    </li>
                    <li>
                      <span id="commits-for-alexandercantrell"></span> commits
                    </li>
                    <li>12 tests</li>
                  </div>
                </div>
                <div class="card" style={{ width: "400px" }} align="left">
                  <div class="card-header" align="center">
                    <h4>Hyuk Jun Choi</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={hyuk}
                    alt="hyuk"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      A Senior at UT Austin studying Computer Science who enjoys
                      playing games and browsing Reddit in his free time.
                    </p>
                    <p>
                      <b>Role:</b> frontend, API/testing (Postman)
                    </p>
                    <li>
                      <span id="issues-for-hjchoi3"></span> assigned issues
                    </li>
                    <li>
                      <span id="commits-for-hjchoi3"></span> commits
                    </li>
                    <li>16 tests</li>
                  </div>
                </div>
                <div class="card" style={{ width: "400px" }} align="left">
                  <div class="card-header" align="center">
                    <h4>David Huang</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={david}
                    alt="david"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      A student at UTCS trying to watch more movies, play more
                      tennis, and read more books.
                    </p>
                    <p>
                      <b>Role:</b> database management, API fetching and
                      serving, and frontend tasks related to rendering data
                    </p>
                    <li>
                      <span id="issues-for-dhuang16"></span> assigned issues
                    </li>
                    <li>
                      <span id="commits-for-dhuang16"></span> commits
                    </li>
                    <li>20 tests</li>
                  </div>
                </div>
              </div>
              <div class="card-deck" style={{ margin: "auto" }}>
                <div
                  class="card"
                  style={{ width: "400px", marginTop: "10px" }}
                  align="left"
                >
                  <div class="card-header" align="center">
                    <h4>Marshall Klansek</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={marshall}
                    alt="marshall"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      A Senior Computer Science major at the University of Texas
                      at Austin who enjoys video games, music, and hanging out
                      with friends.
                    </p>
                    <p>
                      <b>Role:</b>data scraping, frontend format
                    </p>
                    <li>
                      <span id="issues-for-klansekmarshall"></span> assigned
                      issues
                    </li>
                    <li>
                      <span id="commits-for-klansekmarshall"></span> commits
                    </li>
                    <li>0 tests</li>
                  </div>
                </div>
                <div
                  class="card"
                  style={{ width: "400px", marginTop: "10px" }}
                  align="left"
                >
                  <div class="card-header" align="center">
                    <h4>Yujin Seo</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={yujin}
                    alt="yujin"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      A Senior at UT Austin studying Computer Science who enjoys
                      eating, sleeping, and watching movies.
                    </p>
                    <p>
                      <b>Role:</b> frontend, data scrapping
                    </p>
                    <li>
                      <span id="issues-for-yujinseo"></span> assigned issues
                    </li>
                    <li>
                      <span id="commits-for-yujinseo"></span> commits
                    </li>
                    <li>17 tests</li>
                  </div>
                </div>
                <div
                  class="card"
                  style={{ width: "400px", marginTop: "10px" }}
                  align="left"
                >
                  <div class="card-header" align="center">
                    <h4>Jennifer (Yuanhui) Zheng</h4>{" "}
                  </div>
                  <img
                    class="card-img-top"
                    src={jenn}
                    alt="jenn"
                    height={400}
                  />
                  <div style={{ marginLeft: "5px" }}>
                    <p>
                      Hi! I am a UTCS student graduating this semester! I love
                      brunch foods, social dancing, and watching the Rockets
                      play!
                    </p>
                    <p>
                      <b>Role:</b> AWS, database management, and miscellaneous
                      backend tasks
                    </p>
                    <li>
                      <span id="issues-for-jennifer-zheng"></span> assigned
                      issues
                    </li>
                    <li>
                      <span id="commits-for-jennifer-zheng"></span> commits
                    </li>
                    <li>10 tests</li>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </center>
        <div class="row justify-content-center">
          <div
            class="column"
            style={{ width: "250px", marginTop: "50px", marginBottom: "px" }}
          >
            <h4>Total Stats</h4>
            <ul>
              <li>
                <span id="all-issues"></span> total issues
              </li>
              <li>
                <span id="all-commits"></span> commits
              </li>
              <li> 75 tests </li>
            </ul>
          </div>
          <div
            class="column"
            style={{ width: "250px", marginTop: "50px", marginBottom: "100px" }}
          >
            <h4>Data Sources</h4>
            <ul>
              <li>
                <a href="https://www.epa.gov/enviro/sdwis-overview">
                  Water Quality Data
                </a>
              </li>
              <li>
                <a href="https://docs.airnowapi.org/">Air Quality Data</a>
              </li>
              <li>
                <a href="https://ridb.recreation.gov/docs">
                  Parks and Recreation Data
                </a>
              </li>
              <li>
                <a href="https://hifld-geoplatform.opendata.arcgis.com/datasets/6ac5e325468c4cb9b905f1728d6fbf0f_0">
                  Hospital Data
                </a>
              </li>
            </ul>
            Data is scraped via REST API's on their websites.
          </div>
          <div
            class="column"
            style={{ width: "250px", marginTop: "50px", marginBottom: "100px" }}
          >
            <h4>Links</h4>
            <h5>
              <a href="https://gitlab.com/jennifer-zheng/metrohealth">
                GitLab Repo
              </a>
            </h5>
            <h5>
              <a href="https://documenter.getpostman.com/view/10487191/SzKYNc5n">
                Postman API
              </a>
            </h5>
          </div>
        </div>
        <center>
          <h3>Tools</h3>
        </center>
        <div class="container-fluid">
          <div class="row">
            <div class="card-deck" style={{ margin: "auto" }}>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>AWS</h4>{" "}
                </div>
                <img class="card-img-top" src={aws} alt="aws" height={150} />
                <div style={{ marginLeft: "5px" }}>
                  <p>
                    to host our web pages, configure our domain, and manage our
                    database
                  </p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Bootstrap</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={bootstrap}
                  alt="bootstrap"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to style our web pages</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Docker</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={docker}
                  alt="docker"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to standardize our build environment</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Flask</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={flask}
                  alt="flask"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>framework to generate RESTful API calls for our backend</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Gitlab</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={gitlab}
                  alt="gitlab"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>for source control, issue tracker, and pipeline</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Mocha</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={mocha}
                  alt="mocha"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to write our JavaScript unit tests</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="card-deck" style={{ margin: "auto" }}>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>PlantUML</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={plantuml}
                  alt="plantuml"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to model our database</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>PostgreSQL</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={postgresql}
                  alt="postgresql"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>our database management system</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Postman</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={postman}
                  alt="postman"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to design and implement our API</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>React</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={react}
                  alt="react"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to construct our UI</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>Selenium</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={selenium}
                  alt="selenium"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>to write our GUI unit tests</p>
                </div>
              </div>
              <div
                class="card"
                style={{ width: "150", marginTop: "10px" }}
                align="center"
              >
                <div class="card-header" align="center">
                  <h4>SQLAlchemy</h4>{" "}
                </div>
                <img
                  class="card-img-top"
                  src={sqlalchemy}
                  alt="sqlalchemy"
                  height={150}
                />
                <div style={{ marginLeft: "5px" }}>
                  <p>SQL toolkit for our API</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
