import React, { Component } from "react";
import { Helmet } from "react-helmet";
import DontStopFundsVisualizations from "./DontStopFundsVisualizations";

import MetrohealthPieChart from "../visualizations/MetrohealthPieChart";
import MetrohealthChoropleth from "../visualizations/MetrohealthChoropleth";
import MetrohealthBarChart from "../visualizations/MetrohealthBarChart";

const TAB_TITLE = "Visualizations";

class Visualizations extends Component {
  constructor() {
    super();
    this.state = {
      candidate_info: [],
      interest_group_data: []
    };
  }

  componentDidMount() {
    fetch("https://api.thefundsdontstop.me/getAllCandidateInfo")
      .then(response => response.json())
      .then(json => {
        this.setState({
          candidate_info: json
        });
      });

    fetch("https://api.thefundsdontstop.me/getAllInterestGroups")
      .then(response => response.json())
      .then(json => {
        this.setState({
          interest_group_data: json
        });
      });
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>{TAB_TITLE}</title>
        </Helmet>
        <center>
          <h1>Visualizations</h1>
          <div>
            <MetrohealthPieChart />
            <MetrohealthChoropleth />
            <MetrohealthBarChart />
            <h3>Provider's Visualizations</h3>
            <DontStopFundsVisualizations
              candidate_info={this.state.candidate_info}
              interest_group_data={this.state.interest_group_data}
            />
          </div>
        </center>
      </div>
    );
  }
}

export default Visualizations;
