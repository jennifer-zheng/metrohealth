import React, { Component } from "react";
import { Link } from "react-router-dom";
import Pagination from "../Pagination";
import { Helmet } from "react-helmet";
import Filter from "./CityFilterIndex";
import Highlighter from "react-highlight-words";
import CardDisplay from "../CardDisplay";

const TAB_TITLE = "Cities";

class Cities extends Component {
  constructor() {
    super();
    this.state = {
      cities: [],
      currPage: 1,
      totalPages: 1,
      totalInstances: 0,
      queryString: "&",
      keyword: ""
    };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.searchFunction = this.searchFunction.bind(this);
    this.sortTable = this.sortTable.bind(this);
    this.filtering = this.filtering.bind(this);
  }

  filtering() {
    let queryString = "";

    const filterState = document.getElementById("region");
    const sort = document.getElementById("sort");
    const attr = document.getElementById("attribute");
    if (filterState.selectedIndex !== 0) {
      queryString +=
        "&state=" + filterState.options[filterState.selectedIndex].value;
    }

    if (sort.value && attr.value) {
      queryString += "&sort=" + attr.value + sort.value;
    }
    console.log(queryString);
    fetch(
      "https://api.metrohealth.me/api/cities?results_per_page=10" + queryString
    )
      .then(response => response.json())
      .then(json => {
        const cities = json["objects"];
        const totalPages = json["total_pages"];
        const totalInstances = json["num_results"];
        this.setState({
          cities: cities,
          totalPages: totalPages,
          totalInstances: totalInstances,
          queryString: queryString
        });
      });
  }

  componentDidMount() {
    const querystring = this.props.location.search;
    let page = 1;
    let qs = "";
    let searchWord = "";
    if (querystring !== "") {
      const parsedQuerystring = querystring.substring(1);
      const queries = parsedQuerystring.split("&");
      queries.forEach(query => {
        const keyValue = query.split("=");
        if (keyValue.length > 1 && keyValue[0] === "search") {
          searchWord = keyValue[1];
        }
        if (keyValue.length > 1 && keyValue[0] === "page") {
          page = keyValue[1];
        } else if (keyValue.length > 1) {
          qs += "&" + query;
        }
      });
    }

    fetch(
      "https://api.metrohealth.me/api/cities?results_per_page=10&page=" +
        page +
        qs
    )
      .then(response => response.json())
      .then(json => {
        const cities = json["objects"];
        const curr = json["page"];
        const totalPages = json["total_pages"];
        const totalInstances = json["num_results"];
        this.setState({
          cities: cities,
          currPage: curr,
          totalPages: totalPages,
          totalInstances: totalInstances,
          queryString: qs,
          keyword: searchWord
        });
      });
  }

  render() {
    const renderedCities = this.state.cities.map(d => {
      return (
        <tr>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.name}
            />
          </td>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.county}
            />
          </td>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.state}
            />
          </td>
          <td>{d.population}</td>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.air_quality}
            />
          </td>
          <td>{d.water_quality}</td>
          <Link
            to={{
              pathname: `/cities/${d.city_id}`
            }}
          >
            See More
          </Link>
        </tr>
      );
    });

    return (
      <div className="container" style={{ padding: "30px" }}>
        <div></div>
        <div class="card mb-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input
                placeholder="Search for cities, counties, states, etc..."
                onKeyPress={this.handleKeyPress}
                value={this.state.name}
                type="text"
                id="searchInput"
                class="form-control"
              ></input>
              <span class="input-group-btn">
                <button class="btn btn-secondary" onClick={this.searchFunction}>
                  Search
                </button>
              </span>
            </div>
            <Filter />
            <div className="text-center">
              <button className="btn btn-secondary" onClick={this.filtering}>
                Apply Filters
              </button>
            </div>
          </div>
        </div>
        <h2>
          Cities ({this.state.totalInstances} Instances and{" "}
          {this.state.totalPages} Pages)
        </h2>
        <div className="text-center"></div>
        <table className="table table-striped" id="table">
          <thead>
            <tr>
              <th scope="col">
                Name{" "}
                <button onClick={() => this.sortTable(0, false)}>Sort</button>
              </th>
              <th scope="col">
                County{" "}
                <button onClick={() => this.sortTable(1, false)}>Sort</button>
              </th>
              <th scope="col">
                State{" "}
                <button onClick={() => this.sortTable(2, false)}>Sort</button>
              </th>
              <th scope="col">
                Population{" "}
                <button onClick={() => this.sortTable(3, true)}>Sort</button>
              </th>
              <th scope="col">
                Air Quality{" "}
                <button onClick={() => this.sortTable(4, false)}>Sort</button>
              </th>
              <th scope="col">
                Number of Water Safety Violations{" "}
                <button onClick={() => this.sortTable(5, true)}>Sort</button>
              </th>
              <th scope="col">More Info</th>
            </tr>
          </thead>
          <tbody>{renderedCities}</tbody>
        </table>
        <Pagination
          url="/cities"
          query={this.state.queryString}
          current={this.state.currPage}
          lastPage={this.state.totalPages}
        />
        <Helmet>
          <title>{TAB_TITLE}</title>
        </Helmet>
        <CardDisplay items={this.state.cities} url="/cities/" />
      </div>
    );
  }

  handleKeyPress(e) {
    if (e.key === "Enter") {
      this.searchFunction();
    }
  }

  searchFunction() {
    let searchWord = "";
    let qs = "";
    const searchLoc = document.getElementById("searchInput");
    if (searchLoc.value) {
      qs += "&search=" + searchLoc.value;
      searchWord = searchLoc.value;
    }
    fetch(
      "https://api.metrohealth.me/api/cities?results_per_page=10&search=" +
        searchWord
    )
      .then(response => response.json())
      .then(json => {
        const cities = json["objects"];
        const curr = json["page"];
        const totalPages = json["total_pages"];
        const totalInstances = json["num_results"];
        this.setState({
          cities: cities,
          currPage: curr,
          totalPages: totalPages,
          totalInstances: totalInstances,
          queryString: qs,
          keyword: searchWord
        });
      });
  }

  sortTable(colIndex, isNumeric) {
    var table,
      rows,
      switching,
      i,
      x,
      y,
      shouldSwitch,
      dir,
      switchcount = 0;
    table = document.getElementById("table");
    switching = true;
    dir = "asc";
    while (switching) {
      switching = false;
      rows = table.getElementsByTagName("tr");
      for (i = 1; i < rows.length - 1; i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[colIndex];
        y = rows[i + 1].getElementsByTagName("TD")[colIndex];
        if (dir === "asc") {
          if (isNumeric) {
            if (parseInt(x.textContent, 10) > parseInt(y.textContent, 10)) {
              shouldSwitch = true;
              break;
            }
          } else {
            if (x.textContent.toLowerCase() > y.textContent.toLowerCase()) {
              shouldSwitch = true;
              break;
            }
          }
        } else if (dir === "desc") {
          if (isNumeric) {
            if (parseInt(x.textContent, 10) < parseInt(y.textContent, 10)) {
              shouldSwitch = true;
              break;
            }
          } else {
            if (x.textContent.toLowerCase() < y.textContent.toLowerCase()) {
              shouldSwitch = true;
              break;
            }
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount++;
      } else {
        if (switchcount === 0 && dir === "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }
}

export default Cities;
