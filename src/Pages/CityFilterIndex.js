import * as React from "react";
import classnames from "classnames";

class Filter extends React.Component {
  render() {
    const containerClasses = classnames("container", "mb-1");
    const formClasses = classnames("form-horizontal");

    return (
      <div>
        <div className={containerClasses}>
          <form className={formClasses} noValidate>
            <p className="mb-1">Refine your results</p>
            <div className="row">
              <div className="row col-8">
                <div className="column col-3 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="region">
                        State
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="region">
                        <option value="0">Choose...</option>
                        <option value="Texas">Texas</option>
                        <option value="California">California</option>
                        <option value="New York">New York</option>
                        <option value="Florida">Florida</option>
                        <option value="Pennsylvania">Pennsylvania</option>
                        <option value="Illinois">Illinois</option>
                        <option value="Ohio">Ohio</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row col-3">
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="sort">
                        Sorting
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="sort">
                        <option value="">Choose...</option>
                        <option value=":asc">Ascending</option>
                        <option value=":desc">Descending</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="column col-6 col-xs-12">
                  <div className="form-group">
                    <div className="col-3 col-sm-12">
                      <label className="form-label" htmlFor="attribute">
                        Column
                      </label>
                    </div>
                    <div className="col-9 col-sm-12">
                      <select className="form-select" id="attribute">
                        <option value="">Choose...</option>
                        <option value="name">Name</option>
                        <option value="county">County</option>
                        <option value="state">State</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Filter;
