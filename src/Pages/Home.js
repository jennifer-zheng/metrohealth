import React, { Component } from "react";
import splash_image from "../img/splash_image.jpg";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import Highlighter from "react-highlight-words";

const TAB_TITLE = "Metro Health";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      collapse: {
        cities: true,
        outdoor_facilities: true,
        hospitals: true,
        visualizations: true
      },
      keyword: null,
      cities: [],
      outdoor_facilities: [],
      hospitals: [],
      loading: true,
      currentPage: 1,
      numPages: 0
    };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.searchFunction = this.searchFunction.bind(this);
  }

  handleClick = e => {
    let collapse = this.state.collapse;
    collapse[e.target.name] = !collapse[e.target.name];
    this.setState({ collapse });
  };

  render() {
    let cities = this.state.cities.map(d => {
      return (
        <tr>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.name}
            />
          </td>
          <Link
            to={{
              pathname: `/cities/${d.city_id}`
            }}
          >
            See More
          </Link>
        </tr>
      );
    });

    let outdoor_facilities = this.state.outdoor_facilities.map(c => {
      return (
        <tr>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={c.name}
            />
          </td>
          <Link
            to={{
              pathname: `/outdoor_facilities/${c.facility_id}`
            }}
          >
            See More
          </Link>
        </tr>
      );
    });

    let hospitals = this.state.hospitals.map(d => {
      return (
        <tr>
          <td>
            <Highlighter
              highlightClassName="bg-warning"
              searchWords={[this.state.keyword]}
              autoEscape={true}
              textToHighlight={d.name}
            />
          </td>
          <Link
            to={{
              pathname: `/hospitals/${d.id}`
            }}
          >
            See More
          </Link>
        </tr>
      );
    });

    return (
      <div>
        <Helmet>
          <title>{TAB_TITLE}</title>
        </Helmet>
        <p></p>
        <div class="container"></div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <p>
            <img
              src={splash_image}
              alt="Japanese Park - Alex Cantrell"
              height={400}
            />
          </p>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <h1>Metro Health</h1>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <p>
            Welcome to Metro Health! The goal of this site is to provide
            information on factors affecting public health in American cities.
            <br />
            We provide various data points for cities. These data points include
            things like water/air quality, parks/recreation spots, and
            hospitals/emergency centers.
            <br />
            This site is aimed at people who prioritize healthy living.
            <br />
          </p>
        </div>

        <div class="container">
          <div class="input-group">
            <input
              placeholder="Quick Search Across All Models"
              onKeyPress={this.handleKeyPress}
              value={this.state.name}
              type="text"
              id="searchInput"
              class="form-control"
            ></input>
            <span class="input-group-btn">
              <button class="btn btn-secondary" onClick={this.searchFunction}>
                Search
              </button>
            </span>
          </div>
          <tbody>
            <h1>Cities</h1>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Link</th>
                </tr>
              </thead>
              <tbody>{cities}</tbody>
            </table>
            <h1>Outdoor Facilities</h1>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Link</th>
                </tr>
              </thead>
              <tbody>{outdoor_facilities}</tbody>
            </table>
            <h1>Hospitals</h1>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Link</th>
                </tr>
              </thead>
              <tbody>{hospitals}</tbody>
            </table>
          </tbody>
        </div>
      </div>
    );
  }

  handleKeyPress(e) {
    if (e.key === "Enter") {
      this.searchFunction();
    }
  }

  searchFunction() {
    let searchWord = "";
    const searchLoc = document.getElementById("searchInput");
    if (searchLoc.value) {
      searchWord = searchLoc.value;
    }

    const url = `https://api.metrohealth.me/api/`;
    fetch(
      url + "cities?results_per_page=10&page=1&search_homepage=" + searchWord
    )
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          cities: data["objects"],
          loading: false
        });
      })
      .catch(console.log);

    fetch(
      url +
        "outdoor_facilities?results_per_page=10&page=1&search_homepage=" +
        searchWord
    )
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          outdoor_facilities: data["objects"],
          loading: false
        });
      })
      .catch(console.log);

    fetch(
      url + "hospitals?results_per_page=10&page=1&search_homepage=" + searchWord
    )
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          hospitals: data["objects"],
          loading: false
        });
      })
      .catch(console.log);
  }
}

export default Home;
