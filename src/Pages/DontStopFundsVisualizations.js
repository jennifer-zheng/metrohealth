import React, { useState, useEffect } from "react";
import { geoAlbersUsa, geoPath } from "d3-geo";
import { feature } from "topojson-client";
import Chart from "react-google-charts";

const projection = geoAlbersUsa();

const DontStopFundsVisualizations = props => {
  const [geographies, setGeographies] = useState([]);

  var candidate_info = props.candidate_info;
  var interest_group_data = props.interest_group_data;

  useEffect(() => {
    fetch("https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json").then(
      response => {
        if (response.status !== 200) {
          console.log(`There was a problem: ${response.status}`);
          return;
        }
        response.json().then(usadata => {
          setGeographies(feature(usadata, usadata.objects.states).features);
        });
      }
    );
  }, []);

  var state_data = new Map();

  var totalRepub = 0;
  var totalDems = 0;
  var totalIndependent = 0;

  candidate_info.forEach(function(candidate) {
    const state = candidate["state"];
    const party = candidate["party"];
    var partyData = [0, 0];
    if (state_data.has(state)) {
      partyData = state_data.get(state);
    }
    if (party === "D") {
      totalDems++;
      partyData[0] += 1;
    } else if (party === "R") {
      totalRepub++;
      partyData[1] += 1;
    } else {
      totalIndependent++;
    }
    state_data.set(state, partyData);
  });

  var candidate_party_info = [
    ["Party", "Number of Candidates"],
    ["Democrat", totalDems],
    ["Republican", totalRepub],
    ["Independent", totalIndependent]
  ];

  Array.from(state_data.keys()).forEach(function(state) {
    const parties = state_data.get(state);
    var party = "N";
    if (parties[0] > parties[1]) {
      party = "D";
    } else if (parties[1] > parties[0]) {
      party = "R";
    }
    state_data.set(state, party);
  });

  var fills = new Map();
  fills.set("D", "rgba(0, 0, 255, 0.4)");
  fills.set("R", "rgba(255, 0, 0, 0.4)");
  fills.set("N", "rgba(190, 190, 190, 0.4)");
  var xMax = 0;
  var yMax = 0;

  var data = [];
  data.push(["", "D vs R"]);

  interest_group_data.forEach(function(group) {
    const dems = group["dems"];
    const repubs = group["repubs"];
    xMax = Math.max(dems, xMax);
    yMax = Math.max(repubs, yMax);

    if (group["total"] !== -1) {
      data.push([dems, repubs]);
    }
  });

  return (
    <div>
      <center>
        <div align="center">
          <Chart
            style={{ margin: "auto" }}
            width={"90vw"}
            height={"750px"}
            chartType="ScatterChart"
            loader={<div>Loading Chart</div>}
            data={data}
            options={{
              hAxis: {
                title: "Democrat",
                viewWindow: { max: 2000000, min: 0 }
              },
              vAxis: {
                title: "Republican",
                viewWindow: { max: 1000000, min: 0 }
              },
              title: "Democrat vs Republican Funding for Interest Groups",
              legend: "none",
              explorer: {
                keepInBounds: true,
                maxZoomIn: 0.0001
              }
            }}
            rootProps={{ "data-testid": "1" }}
          />
        </div>
      </center>
      <center>
        <h4>Candidate Party Affiliation</h4>
        <div>
          <Chart
            width={"90vw"}
            height={"600px"}
            chartType="PieChart"
            loader={<div>Loading Chart</div>}
            data={candidate_party_info}
            rootProps={{ "data-testid": "2" }}
          />
        </div>
      </center>

      <center>
        <h4>Candidate Party Affiliation by Majority</h4>
      </center>
      <svg width={"100%"} height={600} viewBox="0 0 800 600">
        <g className="states">
          {geographies.map((d, i) => (
            <path
              key={`path-${i}`}
              d={geoPath().projection(projection)(d)}
              className="country"
              fill={fills.get(
                state_data.get(abbrState(d["properties"]["name"]))
              )}
              stroke="#FFFFFF"
              strokeWidth={0.5}
            />
          ))}
        </g>
      </svg>
    </div>
  );
};

export default DontStopFundsVisualizations;

function abbrState(input) {
  var states = [
    ["Arizona", "AZ"],
    ["Alabama", "AL"],
    ["Alaska", "AK"],
    ["Arkansas", "AR"],
    ["California", "CA"],
    ["Colorado", "CO"],
    ["Connecticut", "CT"],
    ["Delaware", "DE"],
    ["Florida", "FL"],
    ["Georgia", "GA"],
    ["Hawaii", "HI"],
    ["Idaho", "ID"],
    ["Illinois", "IL"],
    ["Indiana", "IN"],
    ["Iowa", "IA"],
    ["Kansas", "KS"],
    ["Kentucky", "KY"],
    ["Louisiana", "LA"],
    ["Maine", "ME"],
    ["Maryland", "MD"],
    ["Massachusetts", "MA"],
    ["Michigan", "MI"],
    ["Minnesota", "MN"],
    ["Mississippi", "MS"],
    ["Missouri", "MO"],
    ["Montana", "MT"],
    ["Nebraska", "NE"],
    ["Nevada", "NV"],
    ["New Hampshire", "NH"],
    ["New Jersey", "NJ"],
    ["New Mexico", "NM"],
    ["New York", "NY"],
    ["North Carolina", "NC"],
    ["North Dakota", "ND"],
    ["Ohio", "OH"],
    ["Oklahoma", "OK"],
    ["Oregon", "OR"],
    ["Pennsylvania", "PA"],
    ["Rhode Island", "RI"],
    ["South Carolina", "SC"],
    ["South Dakota", "SD"],
    ["Tennessee", "TN"],
    ["Texas", "TX"],
    ["Utah", "UT"],
    ["Vermont", "VT"],
    ["Virginia", "VA"],
    ["Washington", "WA"],
    ["West Virginia", "WV"],
    ["Wisconsin", "WI"],
    ["Wyoming", "WY"]
  ];

  for (var i = 0; i < states.length; i++) {
    if (states[i][0] === input) {
      return states[i][1];
    }
  }
}
