import React from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const TAB_TITLE = "City | ";
const styles = {
  margin: "50px"
};

class CityInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      city_id: "",
      name: "",
      state_id: "",
      state: "",
      county: "",
      population: "",
      density: "",
      zips: "",
      air_quality: "",
      water_quality: "",
      image_url: "",
      embedded_map: "",
      cities: [],
      hospitals_name: [],
      hospitals_id: [],
      outdoor_facilities_name: [],
      outdoor_facilities_id: []
    };
  }

  componentDidMount() {
    const city_id = this.props.match.params.id;
    fetch("https://api.metrohealth.me/api/cities/" + city_id)
      .then(res => res.json())
      .then(data => {
        this.setState({
          city_id: data["city_id"],
          name: data["name"],
          state_id: data["state_id"],
          state: data["state"],
          county: data["county"],
          population: data["population"],
          density: data["density"],
          zips: data["zips"],
          air_quality: data["air_quality"],
          water_quality: data["water_quality"],
          image_url: data["image_url"],
          embedded_map:
            "https://www.google.com/maps/embed/v1/place?key=AIzaSyCkg3TL33eNMtWeRLzekk_mxthEQ1e1TbM&q=" +
            data["name"] +
            ", " +
            data["state"]
        });
        fetch(
          "https://api.metrohealth.me/api/state_grouped_instances/" +
            data["state_id"]
        )
          .then(res => res.json())
          .then(data => {
            var hospitalID = [];
            var hospitalName = [];
            var outdoorName = [];
            var outdoorID = [];
            fetch(
              "https://api.metrohealth.me/api/hospitals/" + data["hospitals"][0]
            )
              .then(res => res.json())
              .then(hos1Data => {
                hospitalName.push(hos1Data["name"]);
                hospitalID.push(hos1Data["hospital_id"]);
                fetch(
                  "https://api.metrohealth.me/api/hospitals/" +
                    data["hospitals"][1]
                )
                  .then(res => res.json())
                  .then(hos2Data => {
                    hospitalName.push(hos2Data["name"]);
                    hospitalID.push(hos2Data["hospital_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/hospitals/" +
                        data["hospitals"][2]
                    )
                      .then(res => res.json())
                      .then(hos3Data => {
                        hospitalName.push(hos3Data["name"]);
                        hospitalID.push(hos3Data["hospital_id"]);
                        this.setState({
                          cities: data["cities"],
                          hospitals_name: hospitalName,
                          hospitals_id: hospitalID
                        });
                      });
                  });
              });
            fetch(
              "https://api.metrohealth.me/api/outdoor_facilities/" +
                data["outdoor_facilities"][0]
            )
              .then(res => res.json())
              .then(outdoor1 => {
                outdoorName.push(outdoor1["name"]);
                outdoorID.push(outdoor1["facility_id"]);
                fetch(
                  "https://api.metrohealth.me/api/outdoor_facilities/" +
                    data["outdoor_facilities"][1]
                )
                  .then(res => res.json())
                  .then(outdoor2 => {
                    outdoorName.push(outdoor2["name"]);
                    outdoorID.push(outdoor2["facility_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/outdoor_facilities/" +
                        data["outdoor_facilities"][2]
                    )
                      .then(res => res.json())
                      .then(outdoor3 => {
                        outdoorName.push(outdoor3["name"]);
                        outdoorID.push(outdoor3["facility_id"]);
                        this.setState({
                          outdoor_facilities_name: outdoorName,
                          outdoor_facilities_id: outdoorID
                        });
                      });
                  });
              });
          });
      });
  }

  render() {
    return (
      <div className="container">
        <h2>{this.state.name}</h2>
        <div>
          <img
            src={this.state.image_url}
            alt={this.state.image_url}
            height={350}
          />
        </div>
        <p></p>
        <div id="map-container-google-8" class="z-depth-1-half map-container-5">
          <iframe
            src={this.state.embedded_map}
            frameborder="0"
            title="embedded_city_map"
          ></iframe>
        </div>
        <table className="table table-striped" style={styles}>
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Name</th>
              <td>{this.state.name}</td>
            </tr>
            <tr>
              <th scope="row">County</th>
              <td>{this.state.county}</td>
            </tr>
            <tr>
              <th scope="row">State</th>
              <td>{this.state.state}</td>
            </tr>
            <tr>
              <th scope="row">Population</th>
              <td>{this.state.population}</td>
            </tr>
            <tr>
              <th scope="row">Density</th>
              <td>{this.state.density}</td>
            </tr>
            <tr>
              <th scope="row">Air Quality</th>
              <td>{this.state.air_quality}</td>
            </tr>
            <tr>
              <th scope="row">Number of Water Safety Violations</th>
              <td>{this.state.water_quality}</td>
            </tr>
            <tr>
              <th scope="row">Nearby Hospitals</th>
              <td>
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[0]}` }}
                >
                  {this.state.hospitals_name[0]}
                </Link>
                ,
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[1]}` }}
                >
                  {this.state.hospitals_name[1]}
                </Link>
                ,
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[2]}` }}
                >
                  {this.state.hospitals_name[2]}
                </Link>
              </td>
            </tr>
            <tr>
              <th scope="row">Nearby Outdoor Facilities</th>
              <td>
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[0]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[0]}
                </Link>
                ,
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[1]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[1]}
                </Link>
                ,
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[2]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[2]}
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
        <Helmet>
          <title>{TAB_TITLE + this.state.name}</title>
        </Helmet>
      </div>
    );
  }
}

export default CityInstance;
