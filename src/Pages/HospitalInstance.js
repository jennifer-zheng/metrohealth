import React from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const TAB_TITLE = "Hospital | ";
const styles = {
  margin: "50px"
};

class HospitalInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hospital_name: "",
      address: "",
      city: "",
      state: "",
      zip_code: "",
      phone_number: "",
      hospital_type: "",
      hospital_status: "",
      county: "",
      website: "",
      owner: "",
      beds: "",
      trauma: "",
      helipad: "",
      image_url: "",
      embedded_map: "",
      cities_id: [],
      cities_name: [],
      hospitals: [],
      outdoor_facilities_id: [],
      outdoor_facilities_name: []
    };
  }

  componentDidMount() {
    const hospital_id = this.props.match.params.id;
    fetch("https://api.metrohealth.me/api/hospitals/" + hospital_id)
      .then(res => res.json())
      .then(data => {
        this.setState({
          hospital_name: data["name"],
          address: data["address"],
          city: data["city"],
          state: data["state"],
          zip_code: data["zip_code"],
          phone_number: data["phone_number"],
          hospital_type: data["hospital_type"],
          hospital_status: data["status"],
          county: data["county"],
          website: data["website"],
          owner: data["hospital_owner"],
          beds: data["beds"],
          trauma: data["trauma"],
          helipad: data["helipad"],
          image_url: data["image_url"],
          embedded_map:
            "https://www.google.com/maps/embed/v1/place?key=AIzaSyCkg3TL33eNMtWeRLzekk_mxthEQ1e1TbM&q=" +
            data["address"]
        });
        fetch(
          "https://api.metrohealth.me/api/state_grouped_instances/" +
            data["state"]
        )
          .then(res => res.json())
          .then(data => {
            var cityID = [];
            var cityName = [];
            var outdoorName = [];
            var outdoorID = [];
            fetch("https://api.metrohealth.me/api/cities/" + data["cities"][0])
              .then(res => res.json())
              .then(city1 => {
                cityName.push(city1["name"]);
                cityID.push(city1["city_id"]);
                fetch(
                  "https://api.metrohealth.me/api/cities/" + data["cities"][1]
                )
                  .then(res => res.json())
                  .then(city2 => {
                    cityName.push(city2["name"]);
                    cityID.push(city2["city_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/cities/" +
                        data["cities"][2]
                    )
                      .then(res => res.json())
                      .then(city3 => {
                        cityName.push(city3["name"]);
                        cityID.push(city3["city_id"]);
                        this.setState({
                          cities_id: cityID,
                          cities_name: cityName,
                          hospitals: data["hospitals"]
                        });
                      });
                  });
              });
            fetch(
              "https://api.metrohealth.me/api/outdoor_facilities/" +
                data["outdoor_facilities"][0]
            )
              .then(res => res.json())
              .then(outdoor1 => {
                outdoorName.push(outdoor1["name"]);
                outdoorID.push(outdoor1["facility_id"]);
                fetch(
                  "https://api.metrohealth.me/api/outdoor_facilities/" +
                    data["outdoor_facilities"][1]
                )
                  .then(res => res.json())
                  .then(outdoor2 => {
                    outdoorName.push(outdoor2["name"]);
                    outdoorID.push(outdoor2["facility_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/outdoor_facilities/" +
                        data["outdoor_facilities"][2]
                    )
                      .then(res => res.json())
                      .then(outdoor3 => {
                        outdoorName.push(outdoor3["name"]);
                        outdoorID.push(outdoor3["facility_id"]);
                        this.setState({
                          outdoor_facilities_name: outdoorName,
                          outdoor_facilities_id: outdoorID
                        });
                      });
                  });
              });
          });
      });
  }

  render() {
    return (
      <div className="container">
        <h2>{this.state.hospital_name}</h2>
        <div>
          <img
            src={this.state.image_url}
            alt={this.state.image_url}
            height={350}
          />
        </div>
        <p></p>
        <div id="map-container-google-8" class="z-depth-1-half map-container-5">
          <iframe
            src={this.state.embedded_map}
            frameborder="0"
            title="embedded_hospital_map"
          ></iframe>
        </div>
        <table className="table table-striped" style={styles}>
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Name</th>
              <td>{this.state.hospital_name}</td>
            </tr>
            <tr>
              <th scope="row">Address</th>
              <td>{this.state.address}</td>
            </tr>
            <tr>
              <th scope="row">City</th>
              <td>{this.state.city}</td>
            </tr>
            <tr>
              <th scope="row">State</th>
              <td>{this.state.state}</td>
            </tr>
            <tr>
              <th scope="row">Zip Code</th>
              <td>{this.state.zip_code}</td>
            </tr>
            <tr>
              <th scope="row">Phone Number</th>
              <td>{this.state.phone_number}</td>
            </tr>
            <tr>
              <th scope="row">Type</th>
              <td>{this.state.hospital_type}</td>
            </tr>
            <tr>
              <th scope="row">Status</th>
              <td>{this.state.hospital_status}</td>
            </tr>
            <tr>
              <th scope="row">County</th>
              <td>{this.state.county}</td>
            </tr>
            <tr>
              <th scope="row">Website</th>
              <td>{this.state.website}</td>
            </tr>
            <tr>
              <th scope="row">Owner</th>
              <td>{this.state.owner}</td>
            </tr>
            <tr>
              <th scope="row">Beds</th>
              <td>{this.state.beds}</td>
            </tr>
            <tr>
              <th scope="row">Trauma</th>
              <td>{this.state.trauma}</td>
            </tr>
            <tr>
              <th scope="row">Helipad</th>
              <td>{this.state.helipad}</td>
            </tr>
            <tr>
              <th scope="row">Nearby Cities</th>
              <td>
                <Link to={{ pathname: `/cities/${this.state.cities_id[0]}` }}>
                  {this.state.cities_name[0]}
                </Link>
                ,
                <Link to={{ pathname: `/cities/${this.state.cities_id[1]}` }}>
                  {this.state.cities_name[1]}
                </Link>
                ,
                <Link to={{ pathname: `/cities/${this.state.cities_id[2]}` }}>
                  {this.state.cities_name[2]}
                </Link>
              </td>
            </tr>
            <tr>
              <th scope="row">Nearby Outdoor Facilities</th>
              <td>
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[0]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[0]}
                </Link>
                ,
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[1]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[1]}
                </Link>
                ,
                <Link
                  to={{
                    pathname: `/outdoor_facilities/${this.state.outdoor_facilities_id[2]}`
                  }}
                >
                  {this.state.outdoor_facilities_name[2]}
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
        <Helmet>
          <title>{TAB_TITLE + this.state.hospital_name}</title>
        </Helmet>
      </div>
    );
  }
}

export default HospitalInstance;
