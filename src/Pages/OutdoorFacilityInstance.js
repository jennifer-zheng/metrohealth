import React from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const TAB_TITLE = "Outdoor Facility | ";
const styles = {
  margin: "50px"
};

class OutdoorFacilityInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      facility_id: "",
      facility_name: "",
      description: "",
      facility_type: "",
      directions: "",
      phone_number: "",
      email: "",
      ada_access: "",
      reservable: "",
      city: "",
      state: "",
      zip_code: "",
      image_url: "",
      embedded_map: "",
      cities_id: [],
      cities_name: [],
      hospitals_id: [],
      hospitals_name: [],
      outdoor_facilities: []
    };
  }

  componentDidMount() {
    const facility_id = this.props.match.params.id;
    fetch("https://api.metrohealth.me/api/outdoor_facilities/" + facility_id)
      .then(res => res.json())
      .then(data => {
        this.setState({
          facility_id: data["facility_id"],
          facility_name: data["name"],
          description: data["description"],
          facility_type: data["facility_type"],
          directions: data["directions"],
          phone_number: data["phone_number"],
          email: data["email"],
          ada_access: data["ada_access"] === "" ? "N/A" : data["ada_access"],
          reservable: data["reservable"] ? "Yes" : "No",
          city: data["city"],
          state: data["state"],
          zip_code: data["zip_code"],
          image_url: data["image_url"],
          embedded_map:
            "https://www.google.com/maps/embed/v1/place?key=AIzaSyCkg3TL33eNMtWeRLzekk_mxthEQ1e1TbM&q=" +
            data["zip_code"]
        });
        fetch(
          "https://api.metrohealth.me/api/state_grouped_instances/" +
            data["state"]
        )
          .then(res => res.json())
          .then(data => {
            var cityID = [];
            var cityName = [];
            var hospitalName = [];
            var hospitalID = [];
            fetch("https://api.metrohealth.me/api/cities/" + data["cities"][0])
              .then(res => res.json())
              .then(city1 => {
                cityName.push(city1["name"]);
                cityID.push(city1["city_id"]);
                fetch(
                  "https://api.metrohealth.me/api/cities/" + data["cities"][1]
                )
                  .then(res => res.json())
                  .then(city2 => {
                    cityName.push(city2["name"]);
                    cityID.push(city2["city_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/cities/" +
                        data["cities"][2]
                    )
                      .then(res => res.json())
                      .then(city3 => {
                        cityName.push(city3["name"]);
                        cityID.push(city3["city_id"]);
                        this.setState({
                          cities_id: cityID,
                          cities_name: cityName,
                          outdoor_facilities: data["outdoor_facilities"]
                        });
                      });
                  });
              });
            fetch(
              "https://api.metrohealth.me/api/hospitals/" + data["hospitals"][0]
            )
              .then(res => res.json())
              .then(hos1Data => {
                hospitalName.push(hos1Data["name"]);
                hospitalID.push(hos1Data["hospital_id"]);
                fetch(
                  "https://api.metrohealth.me/api/hospitals/" +
                    data["hospitals"][1]
                )
                  .then(res => res.json())
                  .then(hos2Data => {
                    hospitalName.push(hos2Data["name"]);
                    hospitalID.push(hos2Data["hospital_id"]);
                    fetch(
                      "https://api.metrohealth.me/api/hospitals/" +
                        data["hospitals"][2]
                    )
                      .then(res => res.json())
                      .then(hos3Data => {
                        hospitalName.push(hos3Data["name"]);
                        hospitalID.push(hos3Data["hospital_id"]);
                        this.setState({
                          hospitals_name: hospitalName,
                          hospitals_id: hospitalID
                        });
                      });
                  });
              });
          });
      });
  }

  strippedHtml(html) {
    var divElement = document.createElement("div");
    divElement.innerHTML = html;
    return divElement.textContent || divElement.innerText || "";
  }

  render() {
    return (
      <div className="container">
        <h2>{this.state.facility_name}</h2>
        <div>
          <img
            src={this.state.image_url}
            alt={this.state.image_url}
            height={350}
          />
        </div>
        <p></p>
        <div id="map-container-google-8" class="z-depth-1-half map-container-5">
          <iframe
            src={this.state.embedded_map}
            frameborder="0"
            title="embedded_outdoor_facility_map"
          ></iframe>
        </div>
        <table className="table table-striped" style={styles}>
          <thead>
            <tr>
              <th scope="col">Attribute</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Name</th>
              <td>{this.state.facility_name}</td>
            </tr>
            <tr>
              <th scope="row">Description</th>
              <td>{this.strippedHtml(this.state.description)}</td>
            </tr>
            <tr>
              <th scope="row">Type</th>
              <td>{this.state.facility_type}</td>
            </tr>
            <tr>
              <th scope="row">Directions</th>
              <td>{this.strippedHtml(this.state.directions)}</td>
            </tr>
            <tr>
              <th scope="row">Phone Number</th>
              <td>{this.state.phone_number}</td>
            </tr>
            <tr>
              <th scope="row">Email</th>
              <td>{this.state.email}</td>
            </tr>
            <tr>
              <th scope="row">ADA Access</th>
              <td>{this.state.ada_access}</td>
            </tr>
            <tr>
              <th scope="row">Reservable</th>
              <td>{this.state.reservable}</td>
            </tr>
            <tr>
              <th scope="row">City</th>
              <td>{this.state.city}</td>
            </tr>
            <tr>
              <th scope="row">State</th>
              <td>{this.state.state}</td>
            </tr>
            <tr>
              <th scope="row">Zip Code</th>
              <td>{this.state.zip_code}</td>
            </tr>
            <tr>
              <th scope="row">Nearby Cities</th>
              <td>
                <Link to={{ pathname: `/cities/${this.state.cities_id[0]}` }}>
                  {this.state.cities_name[0]}
                </Link>
                ,
                <Link to={{ pathname: `/cities/${this.state.cities_id[1]}` }}>
                  {this.state.cities_name[1]}
                </Link>
                ,
                <Link to={{ pathname: `/cities/${this.state.cities_id[2]}` }}>
                  {this.state.cities_name[2]}
                </Link>
              </td>
            </tr>
            <tr>
              <th scope="row">Nearby Hospitals</th>
              <td>
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[0]}` }}
                >
                  {this.state.hospitals_name[0]}
                </Link>
                ,
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[1]}` }}
                >
                  {this.state.hospitals_name[1]}
                </Link>
                ,
                <Link
                  to={{ pathname: `/hospitals/${this.state.hospitals_id[2]}` }}
                >
                  {this.state.hospitals_name[2]}
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
        <Helmet>
          <title>{TAB_TITLE + this.state.facility_name}</title>
        </Helmet>
      </div>
    );
  }
}

export default OutdoorFacilityInstance;
