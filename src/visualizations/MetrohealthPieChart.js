import React from 'react';
import * as d3 from 'd3';

const QUERY = "https://api.metrohealth.me/api/hospitals"

class MetrohealthPieChart extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      hospitalTypeCount: {}
    };
    this.createPieChart = this.createPieChart.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.createPieChart();
  }

  componentDidUpdate(){
    this.createPieChart();
  }

  getQData(){
    return fetch(QUERY)
      .then(res => res.json())
      .then(data => { 
        var hospitalTypeMap = {};
        var hospitals = data["objects"]
        for (let index in hospitals) {
            var type = hospitals[index]["hospital_type"]
            if (type in hospitalTypeMap) {
                hospitalTypeMap[type] = hospitalTypeMap[type] + 1;
            } else {
                hospitalTypeMap[type] = 1;
            }
        }
        this.setState({hospitalTypeCount : hospitalTypeMap});
    })
  }

  getData(){
    return Promise.all([this.getQData()]);
  }

  createPieChart(){
    var width = 700
    var height = 700
    var margin = 40
    var radius = Math.min(width, height) / 2 - margin

    var svg = d3.select("#pie_chart")
    .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", "0 0 900 900")
    .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var data = this.state.hospitalTypeCount

    var color = d3.scaleOrdinal()
    .domain(data)
    .range(["yellow", "blue", "orange", "black", "slateblue", "brown", "red", "grey", "green", "purple"])

    var pie = d3.pie()
    .value(function(d) {return d.value; })
    var data_ready = pie(d3.entries(data))

    svg
    .selectAll('path')
    .data(data_ready)
    .enter()
    .append('path')
    .attr('d', d3.arc()
        .innerRadius(0)
        .outerRadius(radius)
    )
    .attr('fill', function(d){ return(color(d.data.key)) })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7)
  
    svg.selectAll("mydots")
    .data(data_ready)
    .enter()
    .append("circle")
      .attr("cx", 350)
      .attr("cy", function(d,i){ return -100 + i*25}) 
      .attr("r", 7)
      .style("fill", function(d){ return(color(d.data.key)) })

    svg.selectAll("mylabels")
      .data(data_ready)
      .enter()
      .append("text")
        .attr("x", 370)
        .attr("y", function(d,i){ return -100+ i*25}) 
        .text(function(d){ return d.data.key})
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle")

  }

  render() {
    return(<div align="center"> 
    <h4>Hospitals by Type</h4>
	    <p> This visualization shows the different types of hospitals available in our dataset.</p>
            <svg id='pie_chart'
            width={1000} height={600}>
            </svg>  
        </div>
    );
  }

}

export default MetrohealthPieChart;
