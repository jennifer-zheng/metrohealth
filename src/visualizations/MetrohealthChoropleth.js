import React from 'react';
import * as d3 from 'd3';
import * as topojson from "topojson-client"

var us = require('./states-albers-10m.json');

const QUERY = "https://api.metrohealth.me/api/cities"

class MetrohealthChoropleth extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      waterSafetyViolations: {},
      totalViolations: 0
    };
    this.createChoropleth = this.createChoropleth.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.createChoropleth();
  }

  componentDidUpdate(){
    this.createChoropleth();
  }

  getQData(){
    return fetch(QUERY)
      .then(res => res.json())
      .then(data => { 
        var waterSafetyViolations = {};
        var cities = data["objects"]
        var totalViolations = 0
        for (let index in cities) {
            var state = "" + cities[index]["state"]
            var violations = cities[index]["water_quality"]
            if (state in waterSafetyViolations) {
              waterSafetyViolations[state] = waterSafetyViolations[state] + violations;
            } else {
              waterSafetyViolations[state] = violations;
            }
            totalViolations += violations
        }
        for(let idx in waterSafetyViolations) {
          waterSafetyViolations[idx] = waterSafetyViolations[idx] * 35
        }
        this.setState({waterSafetyViolations : waterSafetyViolations});
        this.setState({totalViolations: totalViolations})
    })
  }

  getData(){
    return Promise.all([this.getQData()]);
  }

  createChoropleth(){
    var width = 500
    var height = 500
    var data = this.state.waterSafetyViolations
    var color = d3.scaleLinear()
    .domain([0, this.state.totalViolations])
    .range(["white", "blue"])
    var path = d3.geoPath()
    const svg = d3.select("#choropleth")
      .append("svg")

    svg.append("g")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


    svg.append("g")
      .selectAll("path")
      .data(topojson.feature(us, us.objects.states).features)
      .join("path")
        .attr("fill", d => color(data[d.properties.name]))
        .attr("d", path)
      .append("title")
        .text(d => `${d.properties.name}
  ${parseInt(data[d.properties.name]/35) + " violations"}`);

    svg.append("path")
        .datum(topojson.mesh(us, us.objects.states, (a, b) => a !== b))
        .attr("fill", "none")
        .attr("stroke", "white")
        .attr("stroke-linejoin", "round")
        .attr("d", path);
  }

  render() {
    return(<div align="center"> 
    <h4>Water Safety Violations by State</h4>
	    <p> This visualization shows the number of water safety violations relative to each state. 
        Hover over each state to see the exact number of violations.</p>
            <svg id='choropleth'
            width={1000} height={700}>
            </svg>  
        </div>
    );
  }

}

export default MetrohealthChoropleth;
