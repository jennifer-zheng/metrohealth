import React from 'react';
import * as d3 from 'd3';

const QUERY = "https://api.metrohealth.me/api/outdoor_facilities"

class MetrohealthBarChart extends React.Component{
constructor(props) {
    super(props);
    this.state = {
      outdoorFacilities: []
    };
    this.createBarChart = this.createBarChart.bind(this);
  }

  componentDidMount(){
    this.getData();
    this.createBarChart();
  }

  componentDidUpdate(){
    this.createBarChart();
  }

  getQData(){
    return fetch(QUERY)
      .then(res => res.json())
      .then(data => { 
        var outdoorFacilities = {};
        var facilities = data["objects"]
        for (let index in facilities) {
            var state = facilities[index]["state"]
            if(state.length === 2) {
              if (state in outdoorFacilities) {
                  outdoorFacilities[state] = outdoorFacilities[state] + 1;
              } else {
                  outdoorFacilities[state] = 1;
              }
            }
        }
       var keys = Object.keys(outdoorFacilities);
       keys.sort();
       var facilityArray = []
        for (let state of keys) {
          facilityArray.push({"name": state, "value": outdoorFacilities[state]})
        }
        this.setState({outdoorFacilities : facilityArray});
    })
  }

  getData(){
    return Promise.all([this.getQData()]);
  }

  createBarChart(){
    var color = "green"
    var height = 500
    var width = 900
    var margin = ({top: 30, right: 0, bottom: 30, left: 40})
    var data = this.state.outdoorFacilities
    
    var x = d3.scaleBand()
      .domain(d3.range(data.length))
      .range([margin.left, width - margin.right])
      .padding(0.1)

    var y = d3.scaleLinear()
      .domain([0, d3.max(data, d => d.value)]).nice()
      .range([height - margin.bottom, margin.top])

    var xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x).tickFormat(i => data[i].name).tickSizeOuter(0))

    var yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, data.format))
      .call(g => g.select(".domain").remove())
      .call(g => g.append("text")
        .attr("x", -margin.left)
        .attr("y", 10)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .text(data.y))

    const svg = d3.select("#bar_chart")
      .append("svg")

    svg.append("g")
        .attr("fill", color)
      .selectAll("rect")
      .data(data)
      .join("rect")
        .attr("x", (d, i) => x(i))
        .attr("y", d => y(d.value))
        .attr("height", d => y(0) - y(d.value))
        .attr("width", x.bandwidth());

    svg.append("g")
        .call(xAxis);

    svg.append("g")
        .call(yAxis);
  }

  render() {
    return(<div align="center"> 
    <h4>Outdoor Facilities by State</h4>
	    <p> This visualization shows the number of outdoor facilities by state.</p>
            <svg id='bar_chart'
            width={1000} height={600}>
            </svg>  
        </div>
    );
  }
}

export default MetrohealthBarChart;
