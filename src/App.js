import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";

import Home from "./Pages/Home";
import Cities from "./Pages/Cities";
import CityInstance from "./Pages/CityInstance";
import Hospitals from "./Pages/Hospitals";
import HospitalInstance from "./Pages/HospitalInstance";
import OutdoorFacilities from "./Pages/OutdoorFacilities";
import OutdoorFacilityInstance from "./Pages/OutdoorFacilityInstance";
import Visualizations from "./Pages/Visualizations";
import About from "./Pages/About";

class App extends Component {
  state = {};

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/" className="title">
            Metro Health
          </Navbar.Brand>
          <Nav className="ml-auto justify-content-between">
            <Nav.Link href="/cities">Cities</Nav.Link>
            <Nav.Link href="/outdoor_facilities">Outdoor Facilities</Nav.Link>
            <Nav.Link href="/hospitals">Hospitals</Nav.Link>
            <Nav.Link href="/visualizations">Visualizations</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
          </Nav>
        </Navbar>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/cities" component={Cities} />
            <Route exact path="/cities/:id" component={CityInstance} />
            <Route exact path="/hospitals" component={Hospitals} />
            <Route exact path="/hospitals/:id" component={HospitalInstance} />
            <Route
              exact
              path="/outdoor_facilities"
              component={OutdoorFacilities}
            />
            <Route
              exact
              path="/outdoor_facilities/:id"
              component={OutdoorFacilityInstance}
            />
            <Route exact path="/visualizations" component={Visualizations} />
            <Route exact path="/about" component={About} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
