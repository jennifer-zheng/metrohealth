import os
import sys
import requests
import json
import unittest
sys.path.append("../../api/")
from metrohealth import create_app
config_name = "testing"
create_app(config_name)
from metrohealth import app

class Unittests(unittest.TestCase):

    def test_api_city(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/cities', headers={"content-Type": "application/json"})
        self.assertEqual(response.status_code, 200)

    def test_api_hospital(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/hospitals', headers={"content-Type": "application/json"})
        self.assertEqual(response.status_code, 200)

    def test_api_outdoor_facilities(self):
        self.app = app.test_client()
        response = self.app.get('/api/outdoor_facilities',
                                headers={"content-Type": "application/json"})
        self.assertEqual(response.status_code, 200)

    def test_api_state_grouped(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/state_grouped_instances', headers={"content-Type": "application/json"})
        self.assertEqual(response.status_code, 200)

    def test_api_city_id(self):
        self.app = app.test_client()
        response = self.app.get('/api/cities/1840021154',
                                headers={"content-Type": "application/json"})
        data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['city_id'], 1840021154)

    def test_api_hospital_id(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/hospitals/0005793230', headers={"content-Type": "application/json"})
        data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['hospital_id'], 5793230)

    def test_api_outdoor_facilities_id(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/outdoor_facilities/258888', headers={"content-Type": "application/json"})
        data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['facility_id'], 258888)

    def test_api_state_grouped_id(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/state_grouped_instances/WA', headers={"content-Type": "application/json"})
        data = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['state_id'], "WA")

    def test__city_instance_num(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/cities', headers={"content-Type": "application/json"})
        data = json.loads(response.data)['objects']
        self.assertEqual(len(data), 451)

    def test_hospital_instance_num(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/hospitals', headers={"content-Type": "application/json"})
        data = json.loads(response.data)['objects']
        self.assertEqual(len(data), 1978)

    def test_outdoor_facilities_instance_num(self):
        self.app = app.test_client()
        response = self.app.get('/api/outdoor_facilities',
                                headers={"content-Type": "application/json"})
        data = json.loads(response.data)['objects']
        self.assertEqual(len(data), 1536)

    def test_city_paging_num(self):
        self.app = app.test_client()
        response = self.app.get('/api/cities?results_per_page=10&page=1')
        items = json.loads(response.data)['objects']
        self.assertEqual(len(items), 10)

    def test_hospital_paging_num(self):
        self.app = app.test_client()
        response = self.app.get('/api/hospitals?results_per_page=10&page=1')
        items = json.loads(response.data)['objects']
        self.assertEqual(len(items), 10)

    def test_outdoor_facilities_paging_num(self):
        self.app = app.test_client()
        response = self.app.get(
            '/api/outdoor_facilities?results_per_page=10&page=1')
        items = json.loads(response.data)['objects']
        self.assertEqual(len(items), 10)

    def test_city_pages_diff(self):
        self.app = app.test_client()
        response_1 = self.app.get('/api/cities?results_per_page=10&page=1')
        response_2 = self.app.get('/api/cities?results_per_page=10&page=2')
        objects_1 = json.loads(response_1.data)['objects']
        objects_2 = json.loads(response_2.data)['objects']
        self.assertEqual(len(objects_1), len(objects_2))
        for i in range(len(objects_1)):
            self.assertNotEqual(objects_1[i]['name'], objects_2[i]['name'])

    def test_hospital_pages_diff(self):
        self.app = app.test_client()
        response_1 = self.app.get('/api/hospitals?results_per_page=10&page=1')
        response_2 = self.app.get('/api/hospitals?results_per_page=10&page=2')
        objects_1 = json.loads(response_1.data)['objects']
        objects_2 = json.loads(response_2.data)['objects']
        self.assertEqual(len(objects_1), len(objects_2))
        for i in range(len(objects_1)):
            self.assertNotEqual(objects_1[i]['name'], objects_2[i]['name'])

    def test_outdoor_facilities_pages_diff(self):
        self.app = app.test_client()
        response_1 = self.app.get(
            '/api/outdoor_facilities?results_per_page=10&page=1')
        response_2 = self.app.get(
            '/api/outdoor_facilities?results_per_page=10&page=2')
        objects_1 = json.loads(response_1.data)['objects']
        objects_2 = json.loads(response_2.data)['objects']
        self.assertEqual(len(objects_1), len(objects_2))
        for i in range(len(objects_1)):
            self.assertNotEqual(objects_1[i]['name'], objects_2[i]['name'])


if __name__ == "__main__":
    unittest.main()
