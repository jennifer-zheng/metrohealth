import React from "react";
import { configure, shallow } from "enzyme";
import waitUntil from "async-wait-until";
import Adapter from "enzyme-adapter-react-16";
import fetch from "isomorphic-fetch";

import Cities from '../src/Pages/Cities';
import Hospitals from '../src/Pages/Hospitals';
import OutdoorFacilities from '../src/Pages/OutdoorFacilities';
import CityInstance from '../src/Pages/CityInstance';
import HospitalInstance from '../src/Pages/HospitalInstance';
import OutdoorFacilityInstance from '../src/Pages/OutdoorFacilityInstance';

var assert = require('chai').assert;

configure({ adapter: new Adapter() });

describe("Cities Tests", function() {

  it("Internal array should be empty at creation", function() {
    const root = shallow(<Cities location={{search: "?page=1"}} />);
    assert.equal(root.state("cities").length, 0);
  });

  it("Initial array not dependent on pagination arg", function() {
    const root = shallow(<Cities location={{search: "?page=2"}} />);
    assert.equal(root.state("cities").length, 0);
  });

  it("API fetch should retrieve ten instances", function(done) {
    const root = shallow(<Cities location={{search: "?page=1"}} />);
    waitUntil(() => root.state("cities").length > 0).then(() => {
      assert.equal
      (root.state("cities").length, 10);
      done();
    });
  });

  it("Search arg should still retrieve instances", function(done) {
    const root = shallow(<Cities location={{search: "?page=1&search=a"}} />);
    waitUntil(() => root.state("cities").length > 0).then(() => {
      assert.isAtLeast
      (root.state("cities").length, 1);
      done();
    });
  });

  it("Instance should be blank at creation", function() {
    const root = shallow(<CityInstance match={{params: {id: "1840021154"}}} />);
    assert.equal(root.state("city_id"), '');
  });

  it("API fetch should retrieve name", function(done) {
    const root = shallow(<CityInstance match={{params: {id: "1840021154"}}} />);
    waitUntil(() => root.state("city_id") != '').then(() => {
      assert.equal(root.state("name"), 'Yakima');
      done();
    });
  });

  it("API fetch should retrieve population", function(done) {
    const root = shallow(<CityInstance match={{params: {id: "1840021154"}}} />);
    waitUntil(() => root.state("city_id") != '').then(() => {
      assert.equal(root.state("population"), 133233);
      done();
    });
  });

  it("API fetch should retrieve enough hospitals to render", function(done) {
    const root = shallow(<CityInstance match={{params: {id: "1840021154"}}} />);
    waitUntil(() => root.state("hospitals_name").length != 0).then(() => {
      assert.isAtLeast(root.state("hospitals_name").length, 3);
      done();
    });

  });

});

describe("Hospitals Tests", function() {

  it("Internal array should be empty at creation", function() {
    const root = shallow(<Hospitals location={{search: "?page=1"}} />);
    assert.equal(root.state("hospitals").length, 0);
  });

  it("Initial array not dependent on pagination arg", function() {
    const root = shallow(<Hospitals location={{search: "?page=2"}} />);
    assert.equal(root.state("hospitals").length, 0);
  });

  it("API fetch should retrieve ten instances", function(done) {
    const root = shallow(<Hospitals location={{search: "?page=1"}} />);
    waitUntil(() => root.state("hospitals").length > 0).then(() => {
      assert.equal
      (root.state("hospitals").length, 10);
      done();
    });
  });

  it("Search arg should still retrieve instances", function(done) {
    const root = shallow(<Hospitals location={{search: "?page=1&search=a"}} />);
    waitUntil(() => root.state("hospitals").length > 0).then(() => {
      assert.isAtLeast
      (root.state("hospitals").length, 1);
      done();
    });
  });

  it("Instance should be blank at creation", function() {
    const root = shallow(<HospitalInstance match={{params: {id: "5793230"}}} />);
    assert.equal(root.state("hospital_name"), '');
  });

  it("API fetch should retrieve name", function(done) {
    const root = shallow(<HospitalInstance match={{params: {id: "5793230"}}} />);
    waitUntil(() => root.state("hospital_name") != '').then(() => {
      assert.equal(root.state("hospital_name"), "CENTRAL VALLEY GENERAL HOSPITAL");
      done();
    });
  });

  it("API fetch should retrieve website", function(done) {
    const root = shallow(<HospitalInstance match={{params: {id: "5793230"}}} />);
    waitUntil(() => root.state("hospital_name") != '').then(() => {
      assert.equal(root.state("website"), "http://www.hanfordhealth.com");
      done();
    });
  });

  it("API fetch should retrieve enough outdoor facilities to render", function(done) {
    const root = shallow(<HospitalInstance match={{params: {id: "5793230"}}} />);
    waitUntil(() => root.state("hospitals").length != 0).then(() => {
      assert.isAtLeast(root.state("hospitals").length, 3);
      done();
    });

  });

});

describe("Outdoor Facilities Tests", function() {

  it("Internal array should be empty at creation", function() {
    const root = shallow(<OutdoorFacilities location={{search: "?page=1"}} />);
    assert.equal(root.state("outdoor_facilities").length, 0);
  });

  it("Initial array not dependent on pagination arg", function() {
    const root = shallow(<OutdoorFacilities location={{search: "?page=2"}} />);
    assert.equal(root.state("outdoor_facilities").length, 0);
  });

  it("API fetch should retrieve ten instances", function(done) {
    const root = shallow(<OutdoorFacilities location={{search: "?page=1"}} />);
    waitUntil(() => root.state("outdoor_facilities").length > 0).then(() => {
      assert.equal
      (root.state("outdoor_facilities").length, 10);
      done();
    });
  });

  it("Search arg should still retrieve instances", function(done) {
    const root = shallow(<OutdoorFacilities location={{search: "?page=1&search=a"}} />);
    waitUntil(() => root.state("outdoor_facilities").length > 0).then(() => {
      assert.isAtLeast
      (root.state("outdoor_facilities").length, 1);
      done();
    });
  });

});
