import unittest
import time
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
# These tests will run on our beta pipeline prior to pushing to prod.
# Therefore, these tests are written for our beta website.
# cities, next, 10 per kind

base_url = "http://www.metrohealth.beta.s3-website-us-east-1.amazonaws.com/"

class MetroHealthGUITests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920x1080")
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def test_home_page_tab_title(self):
        driver = self.driver
        driver.get(base_url)
        self.assertIn("Metro Health", driver.title)
    
    def test_home_page_splash_heading(self):
        driver = self.driver
        driver.get(base_url)
        heading = driver.find_element_by_tag_name('h1')
        self.assertEquals("Metro Health", heading.text)
    
    def test_cities_page_tab_title(self):
        driver = self.driver
        driver.get(base_url+"cities")
        self.assertIn("Cities", driver.title)
        
    def test_cities_page_buttons(self):
        driver = self.driver
        driver.get(base_url+"cities")
        elements = driver.find_elements(By.XPATH, '//button')
        for element in elements:
            element.click()
    
    def test_outdoor_facilities_page_tab_title(self):
        driver = self.driver
        driver.get(base_url+"outdoor_facilities")
        self.assertIn("Outdoor Facilities", driver.title)
    
    def test_outdoor_facilities_page_buttons(self):
        driver = self.driver
        driver.get(base_url+"outdoor_facilities")
        elements = driver.find_elements(By.XPATH, '//button')
        for element in elements:
            element.click()

    def test_hospitals_page_tab_title(self):
        driver = self.driver
        driver.get(base_url+"hospitals")
        self.assertIn("Hospitals", driver.title)
    
    def test_hospital_page_buttons(self):
        driver = self.driver
        driver.get(base_url+"hospitals")
        elements = driver.find_elements(By.XPATH, '//button')
        for element in elements:
            element.click()
    
    def test_about_page_tab_title(self):
        driver = self.driver
        driver.get(base_url+"about")
        self.assertIn("About", driver.title)

    def test_about_page_links(self):
        driver = self.driver
        driver.get(base_url+"about")
        link = driver.find_element_by_link_text('GitLab Repo')
        link.click()
        driver.get(base_url+"about")
        link = driver.find_element_by_link_text('Postman API')
        link.click()
        driver.get(base_url+"about")
        link = driver.find_element_by_link_text('Hospital Data')
        link.click()
        driver.get(base_url+"about")
        link = driver.find_element_by_link_text('Parks and Recreation Data')
        link.click()
        driver.get(base_url+"about")
        link = driver.find_element_by_link_text('Water Quality Data')
        link.click()

    def test_cities_table(self):
        driver = self.driver
        driver.get(base_url+ "cities")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr') #all children
        assert(len(listings)==10)

    def test_cities_table_data(self):
        driver = self.driver
        driver.get(base_url+ "cities")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr')
        response = requests.get("https://api.metrohealth.me/api/cities?results_per_page=10&page=1")
        json_response = response.json()
        items = json_response['objects']
        for index in range(len(listings)):
            listing = listings[index]
            item = items[index]
            assert(listing.find_element(By.XPATH, './/td[1]/span/span').text == item['name'])
            assert(listing.find_element(By.XPATH, './/td[2]/span/span').text == item['county'])
            assert(listing.find_element(By.XPATH, './/td[3]/span/span').text == item['state'])
            assert(float(listing.find_element(By.XPATH, './/td[4]').text) == float(item['population']))
            assert(listing.find_element(By.XPATH, './/td[5]/span/span').text == item['air_quality'])
            assert(float(listing.find_element(By.XPATH, './/td[6]').text) == float(item['water_quality']))

    def test_hospitals_table(self):
        driver = self.driver
        driver.get(base_url+ "hospitals")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr') #all children
        assert(len(listings)==10)

    def test_hospitals_table_data(self):
        driver = self.driver
        driver.get(base_url+ "hospitals")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr')
        response = requests.get("https://api.metrohealth.me/api/hospitals?results_per_page=10&page=1")
        json_response = response.json()
        items = json_response['objects']
        for index in range(len(listings)):
            listing = listings[index]
            item = items[index]
            assert(listing.find_element(By.XPATH, './/td[1]/span/span').text == item['name'])
            assert(listing.find_element(By.XPATH, './/td[2]/span/span').text == item['city'])
            assert(listing.find_element(By.XPATH, './/td[3]/span/span').text == item['state'])
            assert(listing.find_element(By.XPATH, './/td[4]/span/span').text == item['hospital_owner'])
            assert(listing.find_element(By.XPATH, './/td[5]/span/span').text == item['hospital_type'])
            assert(float(listing.find_element(By.XPATH, './/td[6]').text) == float(item['beds']))
    
    def test_outdoor_facilities_table(self):
        driver = self.driver
        driver.get(base_url+ "outdoor_facilities")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr') #all children
        assert(len(listings)==10)

    def test_outdoor_facilities_table_data(self):
        driver = self.driver
        driver.get(base_url+ "outdoor_facilities")
        time.sleep(1)
        parent = driver.find_element(By.XPATH, '//*[@id="table"]')
        table = parent.find_element(By.XPATH, './/tbody')
        listings = table.find_elements(By.XPATH, './/tr')
        response = requests.get("https://api.metrohealth.me/api/outdoor_facilities?results_per_page=10&page=1")
        json_response = response.json()
        items = json_response['objects']
        for index in range(len(listings)):
            listing = listings[index]
            item = items[index]
            assert(listing.find_element(By.XPATH, './/td[1]/span/span').text == item['name'])
            assert(listing.find_element(By.XPATH, './/td[2]/span/span').text == item['facility_type'])
            reservable = listing.find_element(By.XPATH, './/td[3]/span/span').text == 'Yes'
            assert(reservable == item['reservable'])
            assert(listing.find_element(By.XPATH, './/td[4]/span/span').text == item['phone_number'])
            assert(listing.find_element(By.XPATH, './/td[5]/span/span').text == item['city'])
            assert(listing.find_element(By.XPATH, './/td[6]/span/span').text == item['state'])
    
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
