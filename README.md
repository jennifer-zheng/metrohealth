# Metro Health
Name: Yuanhui Zheng
* EID: yz22294
* GitLab ID: jennifer-zheng
* Estimated completion time: 5 hours
* Actual completion time: 10 hours

Name: Alexander Cantrell
* EID: ajc4699
* GitLab ID: alexandercantrell
* Estimated completion time: 4 hours
* Actual completion time: 3 hours

Name: David Huang
* EID: dsh2272
* GitLab ID: dhuang16
* Estimated completion time: 5 hours
* Actual completion time: 3 hours

Name: Hyuk Jun Choi
* EID: hc25854
* GitLab ID: hjchoi3
* Estimated completion time: 5 hours
* Actual completion time: 6 hours

Name: Marshall Klansek
* EID: jmk3958
* GitLab ID: klansekmarshall
* Estimated completion time: 5 hours
* Actual completion time: 5 hours

Name: Yujin Seo
* EID: ys9637
* GitLab ID: yujinseo
* Estimated completion time: 5 hours
* Actual completion time: 4 hours

Git SHA: 75e0b9afa27e63684d1a03a6fc53454becbf7b4f

Project Leader: Hyuk Jun Choi

GitLab Pipelines: https://gitlab.com/jennifer-zheng/metrohealth/pipelines

Website: www.metrohealth.me

Comments: The high-level frontend structure and implementation of pagination, searching, sorting, and filtering was inspired by and adapted from "The Health Odyssey" project of the Fall 2019 semester.
