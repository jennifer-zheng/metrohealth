import psycopg2
import json
import requests

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="metrohealth3", host="metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com")

cur = conn.cursor()

def insert_city(city):

    city_id = city["id"]
    name = city["city"]
    state_id = city["state_id"]
    state = city["state_name"]
    county = city["county_name"]
    population = city["population"]
    density = city["density"]
    zips = city["zips"]
    air_quality = city["air_quality"]
    water_quality = city["water_quality"]

    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + name 
        + ", "
        + state
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": ""}
    response = requests.get(url=url, headers=payload)
    
    image_url = "https://media.defense.gov/2019/Jul/30/2002164249/-1/-1/0/190730-A-HG995-1002.PNG"
    try:
        image_url = json.loads(response.text)["value"][0]["contentUrl"]
    except:
        print("Error with image query:", name + ", " + state)

    # Table Creation
    # CREATE TABLE cities (
    # ID serial PRIMARY KEY,
    # city_id INT,
    # name TEXT,
    # state_id TEXT,
    # state TEXT,
    # county TEXT,
    # population FLOAT,
    # density INT,
    # zips TEXT,
    # air_quality TEXT,
    # water_quality INT,
    # image_url TEXT
    # );


    cur.execute(
        """
        INSERT INTO cities (city_id, name, state_id, state, county, population, density, zips, air_quality, water_quality, image_url)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (city_id, name, state_id, state, county, population, density, zips, air_quality, water_quality, image_url)
    )

with open("cities_data.json") as city_file:
    cities = json.load(city_file)

    for city in cities:
        insert_city(city)

    conn.commit()

cur.close()
conn.close()
