import requests, json, csv
from time import sleep
from datetime import date
import os

aqAPI_key = "na"

def air_quality(zipCode):
	url = "http://www.airnowapi.org/aq/forecast/zipCode/?"
	parameters = {"format":"application/json",
				"date":date.today(),
				"zipCode":zipCode,
				"distance":25,
				"API_KEY":"0B79748F-69EB-4F4A-9036-927604970271"}
	response = requests.get(url, params = parameters)
	if response.status_code == 200:
		data = response.json()
		if data :
			return data[0]['Category']['Name']
	return "Not Available"

def water_quality(city, state):
	url = "https://enviro.epa.gov/enviro/efservice/SDW_CONTAM_VIOL_CITY/geolocation_city/" + city.upper() + "/STATE/" + state + "/csv"
	response = requests.get(url)
	if response.status_code == 200:
		filename = "water_quality_" + city + ".csv"
		file = open(filename, "w")
		file.write(response.text)
		file.close() 
		file = open(filename)
		row_count = sum(1 for line in file)
		file.close()
		os.remove(filename)
		return row_count - 1
	return "Not Available"

def cities():
	data = []
	with open('uscities.csv', 'r') as cities_file:
		cities = csv.DictReader(cities_file)
		for row in cities:
			del row['county_fips']
			del row['county_fips_all']
			del row['county_name_all']
			del row['lat']
			del row['lng']
			del row['source']
			del row['military']
			del row['incorporated']
			del row['timezone']
			del row['ranking']
			if float(row['population']) > 100000.0:
				row['air_quality'] = air_quality(row['zips'].split(' ', 1)[0])
				row['water_quality'] = water_quality(row['city'], row['state_id'])
				data.append(row)
	with open('cities_data.json', 'w') as outfile:
			json.dump(data, outfile, indent = 4)	


if __name__ == '__main__':
	cities()