import requests, json

if __name__ == '__main__':

	features_clean = []

	facility_types_of_interest = {
	'State Parks', 
	# 'Root', 
	'National Fish Hatchery', 
	# 'Rec Area District', 
	# 'Library', 
	# 'Ticket Facility', 
	'Permit', 
	'Campground', 
	# 'Department', 
	# 'Regional District', 
	# 'Cemetery and Memorial', 
	# 'District', 
	'Construction Camp site',
	# 'Region', 
	# 'Visitor Center',
	# 'Federal', 
	# 'Kiosk', 
	# 'Museum', 
	'Facility', 
	'Project', 
	# 'Agency', 
	# 'Archives'
	}

	for offset in range(0, 7500, 50):
		facility_data_url = 'https://ridb.recreation.gov/api/v1/facilities/?apikey=644caed3-aed7-460c-9a7a-7632387d5298' + '&offset=' + str(offset)
		facility_data_response = requests.get(facility_data_url)
		if facility_data_response.status_code == 200:
			facility_data_features = facility_data_response.json()['RECDATA']
			for inst in facility_data_features:
				if inst['FacilityID'] == "" or inst['FacilityName'] == "" or inst['FacilityDescription'] == "" or inst['FacilityDirections'] == "" or inst['FacilityPhone'] == "" or inst['FacilityEmail'] == "":
					continue
				if inst['FacilityTypeDescription'] in facility_types_of_interest:
					facility_location_url = 'https://ridb.recreation.gov/api/v1/facilities/' + inst['FacilityID'] + '/facilityaddresses/?apikey=644caed3-aed7-460c-9a7a-7632387d5298'
					facility_location_response = requests.get(facility_location_url)
					if facility_location_response.status_code == 200:
						facility_location_features = facility_location_response.json()['RECDATA']
						if (len(facility_location_features) and facility_location_features[0]['City']): # we only really care about city, but postal code and state code are nice
							inst['City'] = facility_location_features[0]['City'].strip()
							inst['PostalCode'] = facility_location_features[0]['PostalCode'].strip()
							inst['AddressStateCode'] = facility_location_features[0]['AddressStateCode'].strip()
							features_clean.append(inst)
	
	with open('outdoor_facilities.json', 'w') as outfile:
			json.dump(features_clean, outfile, indent = 4)
