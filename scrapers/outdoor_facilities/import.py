import psycopg2
import json
import requests

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="metrohealth3", host="metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com")

cur = conn.cursor()

def insert_outdoor_facility(outdoor_facility):

    facility_id = outdoor_facility["FacilityID"]
    name = outdoor_facility["FacilityName"]
    description = outdoor_facility["FacilityDescription"]
    facility_type = outdoor_facility["FacilityTypeDescription"]
    directions = outdoor_facility["FacilityDirections"]
    phone_number = outdoor_facility["FacilityPhone"]
    email = outdoor_facility["FacilityEmail"]
    ada_access = outdoor_facility["FacilityAdaAccess"]
    reservable = outdoor_facility["Reservable"]
    city = outdoor_facility["City"]
    state = outdoor_facility["AddressStateCode"]
    zip_code = outdoor_facility["PostalCode"]

    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + name 
        + ", "
        + state
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": "bb88058d3e1d41cdaf0bf9ec09fc8855"}
    response = requests.get(url=url, headers=payload)
    
    image_url = "https://media.defense.gov/2019/Jul/30/2002164249/-1/-1/0/190730-A-HG995-1002.PNG"
    try:
        image_url = json.loads(response.text)["value"][0]["contentUrl"]
    except:
        print("Error with image query:", name + ", " + state)

    # Table Creation
    # CREATE TABLE outdoor_facilities (
    #    ID serial PRIMARY KEY,
    #    facility_id INT,
    #    name TEXT,
    #    description TEXT,
    #    facility_type TEXT,
    #    directions TEXT,
    #    phone_number TEXT,
    #    email TEXT,
    #    ada_access TEXT,
    #    reservable BOOLEAN,
    #    city TEXT,
    #    state TEXT,
    #    zip_code TEXT,
    #    image_url TEXT
    # );

    cur.execute(
        """
        INSERT INTO outdoor_facilities (facility_id, name, description, facility_type, directions, phone_number, email, ada_access, reservable, city ,state, zip_code, image_url)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (facility_id, name, description, facility_type, directions, phone_number, email, ada_access, reservable, city, state, zip_code, image_url)
    )

with open("outdoor_facilities_data.json") as outdoor_facilities_file:
    outdoor_facilities = json.load(outdoor_facilities_file)

    for outdoor_facility in outdoor_facilities:
        insert_outdoor_facility(outdoor_facility)

    conn.commit()

cur.close()
conn.close()