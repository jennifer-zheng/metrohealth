import psycopg2
import json
import requests

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="metrohealth3", host="metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com")

cur = conn.cursor()

def insert_hospital(hospital):

    hospital_id = hospital["ID"]
    name = hospital["NAME"]
    address = hospital["ADDRESS"]
    city = hospital["CITY"]
    state = hospital["STATE"]
    zip_code = hospital["ZIP"]
    phone_number = hospital["TELEPHONE"]
    hospital_type = hospital["TYPE"]
    status = hospital["STATUS"]
    county = hospital["COUNTY"]
    website = hospital["WEBSITE"]
    hospital_owner = hospital["OWNER"]
    beds = hospital["BEDS"]
    trauma = hospital["TRAUMA"]
    helipad = hospital["HELIPAD"]

    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + name 
        + ", "
        + state
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": ""}
    response = requests.get(url=url, headers=payload)
    
    image_url = "https://media.defense.gov/2019/Jul/30/2002164249/-1/-1/0/190730-A-HG995-1002.PNG"
    try:
        image_url = json.loads(response.text)["value"][0]["contentUrl"]
    except:
        print("Error with image query:", name + ", " + state)

    # Table Creation
	# CREATE TABLE hospitals (
	# ID serial PRIMARY KEY,
	# hospital_id INT,
	# name TEXT,
	# address TEXT,
	# city TEXT,
	# state TEXT,
	# zip_code INT,
	# phone_number TEXT,
	# hospital_type TEXT,
	# status TEXT,
	# county TEXT,
	# website TEXT,
	# hospital_owner TEXT,
	# beds INT,
	# trauma TEXT,
	# helipad TEXT
	# );


    cur.execute(
        """
        INSERT INTO hospitals (hospital_id, name, address, city, state, zip_code, phone_number, hospital_type, status, county, website, hospital_owner, beds, trauma, helipad, image_url)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """,
        (hospital_id, name, address, city, state, zip_code, phone_number, hospital_type, status, county, website, hospital_owner, beds, trauma, helipad, image_url)
    )

with open("hospital_data.json") as hospital_file:
    hospitals = json.load(hospital_file)

    for hospital in hospitals:
        insert_hospital(hospital)

    conn.commit()

cur.close()
conn.close()