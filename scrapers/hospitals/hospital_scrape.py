import requests, json

if __name__ == '__main__':
	url = "https://services1.arcgis.com/Hp6G80Pky0om7QvQ/arcgis/rest/services/Hospitals_1/FeatureServer/0/query?where=1%3D1&outFields=ID,NAME,ADDRESS,CITY,STATE,ZIP,TELEPHONE,TYPE,STATUS,COUNTY,COUNTRY,WEBSITE,OWNER,BEDS,TRAUMA,HELIPAD&returnGeometry=false&outSR=4326&f=json"
	response = requests.get(url)
	if response.status_code == 200:
		data = response.json()
		features = data['features']
		features_clean = []
		for inst in features:
			if inst['attributes']['COUNTRY'] == "USA":
				features_clean.append(inst['attributes'])
		with open('hospital_data.json', 'w') as outfile:
	  		json.dump(features_clean, outfile, indent = 4)