import psycopg2
import requests
import json

state_to_cities = {}
state_to_hospitals = {}
state_to_outdoor_facilites = {}

try:
    connection = psycopg2.connect(user="postgres",
                                password="metrohealth3",
                                host="metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com",
                                database="postgres")
    cursor = connection.cursor()
    postgreSQL_select_Query = "select * from cities"

    cursor.execute(postgreSQL_select_Query)
    records = cursor.fetchall() 
   
    for row in records:
        city_id = row[1]
        state_id = row[3]
        if state_id in state_to_cities.keys():
            city_list = state_to_cities[state_id]
            city_list.append(city_id)
        else:
            state_to_cities[state_id] = [city_id]
    
    postgreSQL_select_Query = "select * from hospitals"

    cursor.execute(postgreSQL_select_Query)
    records = cursor.fetchall() 
   
    for row in records:
        hospital_id = row[1]
        state_id = row[5]
        if state_id in state_to_hospitals.keys():
            hospital_list = state_to_hospitals[state_id]
            hospital_list.append(hospital_id)
        else:
            state_to_hospitals[state_id] = [hospital_id]
    
    postgreSQL_select_Query = "select * from outdoor_facilities"

    cursor.execute(postgreSQL_select_Query)
    records = cursor.fetchall() 
   
    for row in records:
        facility_id = row[1]
        state_id = row[11]
        if state_id in state_to_outdoor_facilites.keys():
            hospital_list = state_to_outdoor_facilites[state_id]
            hospital_list.append(facility_id)
        else:
            state_to_outdoor_facilites[state_id] = [facility_id]
    
except (Exception, psycopg2.Error) as error :
    print ("Error while fetching data from PostgreSQL", error)

finally:
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")

###########################################################################

conn = psycopg2.connect(dbname="postgres", user="postgres",
                        password="metrohealth3", host="metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com")

cur = conn.cursor()

def insert_state_data(state):

    cities = []
    hospitals = []
    outdoor_facilities = []

    if state in state_to_cities.keys():
        cities = state_to_cities[state]
    if state in state_to_hospitals.keys():
        hospitals = state_to_hospitals[state]
    if state in state_to_outdoor_facilites.keys():
        outdoor_facilities = state_to_outdoor_facilites[state]
    
    # CREATE TABLE state_grouped_instances (
    #    ID serial PRIMARY KEY,
    #    state_id TEXT,
    #    cities TEXT [],
    #    hospitals TEXT [],
    #    outdoor_facilities TEXT []
    # );

    cur.execute(
        """
        INSERT INTO state_grouped_instances (state_id, cities, hospitals, outdoor_facilities)
        VALUES (%s, %s, %s, %s);
        """,
        (state, cities, hospitals, outdoor_facilities)
    )

for key in state_to_cities.keys():
    insert_state_data(key)

    conn.commit()

cur.close()
conn.close()
print("PostgreSQL connection is closed")



