import os

from werkzeug.serving import run_with_reloader
from werkzeug.debug import DebuggedApplication
from gevent.pywsgi import WSGIServer
from flask import current_app
from flask_script import Manager, Command, prompt_bool
from metrohealth import create_app
import datetime

config_name = os.getenv('FLASK_CONFIG')
if config_name == None:
    config_name = 'development'
print(config_name)
manager = Manager(create_app(config_name))


@manager.command
def runserver():
    "run the server"
    from metrohealth import app

    def run_server():
        http_server = WSGIServer(
            (app.config["HOST"], app.config["PORT"]), DebuggedApplication(app))
        http_server.serve_forever()
    run_with_reloader(run_server)


@manager.command
def createtables():
    "create all database tables"
    db = current_app.db
    db.create_all()


@manager.command
def build():
    "verifies the app builds properly"
    print("built")


if __name__ == "__main__":
    manager.run()
