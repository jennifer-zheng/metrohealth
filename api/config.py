import os


class Config(object):
    # common configs
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:metrohealth3@metrohealth.crffxcyh679e.us-east-1.rds.amazonaws.com"


class DevelopmentConfig(Config):
    # dev configs
    DEBUG = True
    ENFORCE_SSL = False
    HOST = 'localhost'
    PORT = 5000


class TestingConfig(Config):
    # test configs
    DEBUG = True
    ENFORCE_SSL = False
    HOST = '0.0.0.0'
    PORT = 5000


class ProductionConfig(Config):
    DEBUG = False
    ENFORCE_SSL = True
    HOST = '0.0.0.0'
    PORT = 5000


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
