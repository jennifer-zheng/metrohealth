# app/__init__.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager
import config
from config import app_config

app = None


def create_app(config_name):
    global app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.db = SQLAlchemy(app)
    from . import models
    app.manager = APIManager(app, flask_sqlalchemy_db=app.db)
    from . import views
    return app
