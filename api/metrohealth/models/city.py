from flask import current_app
from metrohealth import app
from flask_sqlalchemy import SQLAlchemy

db = app.db


class City(db.Model):
    __tablename__ = "cities"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    city_id = db.Column(db.Integer, nullable=True, unique=False)
    name = db.Column(db.String(), nullable=True, unique=False)
    state_id = db.Column(db.String(), nullable=True, unique=False)
    state = db.Column(db.String(), nullable=True, unique=False)
    county = db.Column(db.String(), nullable=True, unique=False)
    population = db.Column(db.Float, nullable=True, unique=False)
    density = db.Column(db.Integer, nullable=True, unique=False)
    zips = db.Column(db.String(), nullable=True, unique=False)
    air_quality = db.Column(db.String(), nullable=True, unique=False)
    water_quality = db.Column(db.Integer, nullable=True, unique=False)
    image_url = db.Column(db.String(), nullable=True, unique=False)

    def __repr__(self):
        return "Outdoor_Facility('{}','{}','{}','{}','{}','{}','{}','{}',\
                '{}','{}','{}','{}')".format(self.id, self.city_id, self.name,
                                             self.state_id, self.density, self.zips, self.air_quality,
                                             self.water_quality, self.image_url)
