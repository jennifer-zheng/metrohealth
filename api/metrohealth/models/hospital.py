from flask import current_app
from metrohealth import app
from flask_sqlalchemy import SQLAlchemy

db = app.db


class Hospital(db.Model):
    __tablename__ = "hospitals"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    hospital_id = db.Column(db.Integer, nullable=True, unique=False)
    name = db.Column(db.String(), nullable=True, unique=False)
    address = db.Column(db.String(), nullable=True, unique=False)
    city = db.Column(db.String(), nullable=True, unique=False)
    state = db.Column(db.String(), nullable=True, unique=False)
    zip_code = db.Column(db.Integer, nullable=True, unique=False)
    phone_number = db.Column(db.String(), nullable=True, unique=False)
    hospital_type = db.Column(db.String(), nullable=True, unique=False)
    status = db.Column(db.String(), nullable=True, unique=False)
    county = db.Column(db.String(), nullable=True, unique=False)
    website = db.Column(db.String(), nullable=True, unique=False)
    hospital_owner = db.Column(db.String(), nullable=True, unique=False)
    beds = db.Column(db.Integer, nullable=True, unique=False)
    trauma = db.Column(db.String(), nullable=True, unique=False)
    helipad = db.Column(db.String(), nullable=True, unique=False)
    image_url = db.Column(db.String(), nullable=True, unique=False)

    def __repr__(self):
        return "Hospital('{}','{}','{}','{}','{}','{}','{}','{}','{}',\
            '{}','{}','{}','{}','{}','{}','{}','{}')".format(self.id, self.hospital_id,
                                                             self.name, self.address, self.city, self.state, self.zip_code, self.phone_number,
                                                             self.hospital_type, self.status, self.county, self.website, self.hospital_owner,
                                                             self.beds, self.trauma, self.helipad, self.image_url)
