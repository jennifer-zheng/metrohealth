from flask import current_app
from metrohealth import app
from flask_sqlalchemy import SQLAlchemy

db = app.db


class State_Grouped_Instance(db.Model):
    __tablename__ = "state_grouped_instances"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    state_id = db.Column(db.String(), nullable=True, unique=False)
    cities = db.Column(db.String(), nullable=True, unique=False)
    hospitals = db.Column(db.String(), nullable=True, unique=False)
    outdoor_facilities = db.Column(db.String(), nullable=True, unique=False)
