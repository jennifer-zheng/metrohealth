from flask import current_app
from metrohealth import app
from flask_sqlalchemy import SQLAlchemy

db = app.db


class Outdoor_Facility(db.Model):
    __tablename__ = "outdoor_facilities"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    facility_id = db.Column(db.Integer, nullable=True, unique=False)
    name = db.Column(db.String(), nullable=True, unique=False)
    description = db.Column(db.String(), nullable=True, unique=False)
    facility_type = db.Column(db.String(), nullable=True, unique=False)
    directions = db.Column(db.String(), nullable=True, unique=False)
    phone_number = db.Column(db.String(), nullable=True, unique=False)
    email = db.Column(db.String(), nullable=True, unique=False)
    ada_access = db.Column(db.String(), nullable=True, unique=False)
    reservable = db.Column(db.Boolean, nullable=True, unique=False)
    city = db.Column(db.String(), nullable=True, unique=False)
    state = db.Column(db.String(), nullable=True, unique=False)
    zip_code = db.Column(db.String(), nullable=True, unique=False)
    image_url = db.Column(db.String(), nullable=True, unique=False)

    def __repr__(self):
        return "Outdoor_Facility('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',\
                '{}','{}','{}','{}')".format(self.id, self.facility_id, self.name,
                                             self.description, self.facility_type, self.directions, self.phone_number,
                                             self.email, self.ada_access, self.reservable, self.city, self.state,
                                             self.zip_code, self.image_url)
