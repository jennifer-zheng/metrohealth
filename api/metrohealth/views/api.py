import flask
from metrohealth import app


@app.after_request
def apply_caching(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return response

# Build json string for filtering


def build_filter(field, op, val):
    val = '%'+val+'%'
    return {u'name': field, u'op': op, u'val': val}


def build_outer_filter(field, op, val):
    return {u'name': field, u'op': op, u'val': val}

# Build json string for sorting


def build_sort(field, direction):
    return {u'field': field, u'direction': direction}

# Build search query


def get_columns():
    if "hospitalsapi0.hospitalsapi" == flask.request.endpoint:
        return ['name', 'address', 'city', 'state', 'hospital_type', 'status', 'county', 'website', 'hospital_owner']
    elif "outdoor_facilitiesapi0.outdoor_facilitiesapi" == flask.request.endpoint:
        return ['name', 'description', 'facility_type', 'directions', 'phone_number', 'email', 'city', 'state', 'zip_code']
    elif "citiesapi0.citiesapi" == flask.request.endpoint:
        return ['name', 'county', 'state_id', 'state']


def build_search(val):
    query = []
    columns = get_columns()

    # Build search query
    for field in columns:
        query.append(build_filter(field, "ilike", val))

    return query


def build_homepage_search(val):
    query = []
    columns = get_homepage_columns()

    # Build search query
    for field in columns:
        query.append(build_filter(field, "ilike", val))

    return query


def get_homepage_columns():
    if "hospitalsapi0.hospitalsapi" == flask.request.endpoint:
        return ['name']
    elif "outdoor_facilitiesapi0.outdoor_facilitiesapi" == flask.request.endpoint:
        return ['name']
    elif "citiesapi0.citiesapi" == flask.request.endpoint:
        return ['name']


def build_queries(search_params=None, **kw):
    filters = []
    searches = []
    sorts = []
    for i in flask.request.args.to_dict():
        if i == 'q':
            break
        else:
            # Build sort queries
            if str(i) == 'sort':
                if ':' not in flask.request.args[i]:
                    sorts.append(build_sort(flask.request.args[i], "asc"))
                else:
                    sort_args = flask.request.args[i].split(':')
                    sorts.append(build_sort(sort_args[0], sort_args[1]))
            # Build search queries
            elif str(i) == 'search':
                searches += build_search(flask.request.args[i])
            elif str(i) == 'search_homepage':
                print('abc')
                searches += build_homepage_search(flask.request.args[i])
            # Build operator queries
            elif '<' in str(i):
                filters.append(build_outer_filter(
                    str(i)[:-1], "le", flask.request.args[i]))
            elif '>' in str(i):
                filters.append(build_outer_filter(
                    str(i)[:-1], "ge", flask.request.args[i]))
            # Build filter queries
            elif str(i) != 'page' and str(i) != 'results_per_page':
                columns = get_columns()
                if str(i) in columns:
                    filters.append(build_filter(
                        str(i), "ilike", flask.request.args[i]))
                else:
                    for field in columns:
                        if field[0] == str(i):
                            filters.append(build_outer_filter(field[0], "any", build_filter(
                                field[1], "ilike", flask.request.args[i])))

    if len(searches) > 0:
        filters.append({u'or': searches})
    if len(filters) > 0:
        search_params['filters'] = filters
    if len(sorts) > 0:
        search_params['order_by'] = sorts
