from metrohealth import app
from metrohealth.models import Hospital
from metrohealth.views.api import build_queries
manager = app.manager

# Create api/hospitals endpoint
manager.create_api(
    Hospital,
    collection_name="hospitals",
    results_per_page=0,
    primary_key="hospital_id",
    methods=["GET"],
    preprocessors={
        'GET_MANY': [build_queries]
    }
)
