from metrohealth import app
from metrohealth.models import City
from metrohealth.views.api import build_queries
manager = app.manager

# Create api/cities endpoint
manager.create_api(
    City,
    collection_name="cities",
    results_per_page=0,
    primary_key="city_id",
    methods=["GET"],
    preprocessors={
        'GET_MANY': [build_queries]
    }
)
