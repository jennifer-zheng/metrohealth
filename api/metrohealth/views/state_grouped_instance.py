from metrohealth import app
from metrohealth.models import State_Grouped_Instance
manager = app.manager

# Create api/cities endpoint
manager.create_api(
    State_Grouped_Instance,
    collection_name="state_grouped_instances",
    results_per_page=0,
    primary_key="state_id",
    methods=["GET"],
)
