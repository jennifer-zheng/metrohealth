from metrohealth import app
from metrohealth.models import Outdoor_Facility
from metrohealth.views.api import build_queries
manager = app.manager

# Create api/outdoor_facilities endpoint
manager.create_api(
    Outdoor_Facility,
    collection_name="outdoor_facilities",
    results_per_page=0,
    primary_key="facility_id",
    methods=["GET"],
    preprocessors={
        'GET_MANY': [build_queries]
    }
)
